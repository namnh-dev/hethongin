<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json_dvhc_file = file_get_contents(resource_path('data/dvhcvn.json'));
        $data_dvhc = json_decode($json_dvhc_file);
        foreach ($data_dvhc as $data) {
            DB::table('city')->insert([
                'city_id' => intval(ltrim($data->city_id, '0')),
                'name' => $data->name,
                'type' => $data->type
            ]);
        }
    }
}

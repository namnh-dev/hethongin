<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json_dvhc_file = file_get_contents(resource_path('data/dvhcvn.json'));
        $data_dvhc = json_decode($json_dvhc_file);
        foreach ($data_dvhc as $data_province) {
            foreach ($data_province->province as $data_ward) {
                foreach ($data_ward->ward as $data) {
                    DB::table('ward')->insert([
                        'ward_id' => intval(ltrim($data->ward_id, '0')),
                        'name' => $data->name,
                        'type' => $data->type,
                        'province_id' => intval(ltrim($data_ward->province_id, '0'))
                    ]);
                }
            }
        }
    }
}

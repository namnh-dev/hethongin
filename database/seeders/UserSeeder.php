<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $limit = 10;

        $startDate = Carbon::createFromFormat('Y-m-d', '1970-01-01');
        $endDate = Carbon::createFromFormat('Y-m-d', '2005-12-31');

        $arrayPhoneNum = [2, 3, 8, 9];

        for ($i = 0; $i < $limit; $i++) {
            $phoneNumber = "0" . $arrayPhoneNum[array_rand($arrayPhoneNum)] . $faker->numerify('########');
            DB::table('user')->insert([
                'fullname' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'phone' => $phoneNumber,
                'username' => Str::lower(Str::random(8)),
                'password' => bcrypt('123456'),
                'gender' => rand(0, 1),
                'dob' => Carbon::instance($faker->dateTimeBetween($startDate, $endDate)),
                'address' => $faker->address,
                'city_id' => null,
                'province_id' => null,
                'ward_id' => null,
                'status' => rand(0, 1),
                'created_at' => now()
            ]);
        }
    }
}

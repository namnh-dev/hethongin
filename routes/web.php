<?php

use App\Http\Controllers\AppController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CalculateController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Main Page Route
Route::get('/', [DashboardController::class, 'dashboardEcommerce'])->name('dashboard-ecommerce');
Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::get('/register', [AuthController::class, 'register'])->name('register');
Route::get('/forgot-password', [AuthController::class, 'forgot_password'])->name('forgot-password');
Route::post('/new-register', [AuthController::class, 'new_register'])->name('new-register');
Route::post('/login-now', [AuthController::class, 'login_now'])->name('login-now');
Route::get('/api/check-auth', function() {
    $authenticated = Auth::check();
    $fullname = Auth::user()->fullname;
    return response()->json(['authenticated' => $authenticated, 'fullname' => $fullname]);
  });
  

/* Route Dashboards */
Route::middleware(['auth'])->group(function () {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('analytics', [DashboardController::class, 'dashboardAnalytics'])->name('dashboard-analytics');
        Route::get('ecommerce', [DashboardController::class, 'dashboardEcommerce'])->name('dashboard-ecommerce');
    });
    /* Route Dashboards */

    /* Route Calculate */
    Route::group(['prefix' => 'calculate'], function () {
        Route::get('/calculate-print', [CalculateController::class, 'calculate_print'])->name('calculate-print');
        Route::post('/printing', [CalculateController::class, 'printing'])->name('printing');
    });
    /* Route Calculate */

    /* Route Quản lý */
    Route::group(['prefix' => 'quan-ly'], function () {
        Route::get('/tai-khoan', [AppController::class, 'user_view_account'])->name('quan-ly-tai-khoan');
        Route::get('/bao-mat', [AppController::class, 'user_view_security'])->name('quan-ly-bao-mat');
        Route::get('/nha-in', [AppController::class, 'user_manage_factory'])->name('quan-ly-nha-in');
    });

    Route::post('/edit-profile', [AuthController::class, 'edit_profile'])->name('edit-profile');
    Route::post('/change-password', [AuthController::class, 'change_password'])->name('change-password');
    /* Route Quản lý */

    Route::get('/goi-tai-khoan', [AppController::class, 'user_manage_pricing'])->name('goi-tai-khoan');

    Route::get('/recharge-thueapi', [AppController::class, 'recharge_thueapi'])->name('recharge-thueapi');
    Route::get('/thueapi/callback', [AppController::class, 'callback_thueapi'])->name('callback-thueapi');

    /* Route Cấu hình */
    Route::group(['prefix' => 'cau-hinh'], function () {
        Route::get('/nha-xuong', [AppController::class, 'config_factory'])->name('cau-hinh-nha-xuong');
        Route::get('/don-gia', [AppController::class, 'config_price'])->name('cau-hinh-don-gia');
    });
    /* Route Cấu hình */
});
// locale Route
Route::get('lang/{locale}', [LanguageController::class, 'swap']);

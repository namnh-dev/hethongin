<?php

return [
    "Bảng điều khiển" => "Bảng điều khiển",
    "Quản lý tài khoản" => "Quản lý tài khoản",
    "Hệ thống tính toán" => "Hệ thống tính toán",
    "Tính toán nâng cao" => "Tính toán nâng cao",
    "Tính toán giá in" => "Tính toán giá in",
    "Tính vật tư" => "Tính vật tư",
    "Xếp hình nhanh" => "Xếp hình nhanh",
    "Hình toán tự động" => "Hình toán tự động",
    "Gói tài khoản" => "Gói tài khoản",
    "Cấu hình nhà xưởng" => "Cấu hình nhà xưởng",
    "Trang cá nhân nhà in" => "Trang cá nhân nhà in",
    "Dashboards" => "Dashboards"
];

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'register_fullname' => 'required|string|max:50',
            'register_username' => 'required|max:50|min:6|unique:user,username',
            'register_password' => 'required|min:8|regex:/^(?=.*[A-Z])(?=.*[\W])/',
            'register_confirm_password' => 'required|same:register_password',
            'register_email' => 'required|email|max:100|unique:user,email',
            'register_phone' => 'required|unique:user,phone'
        ];
        
        return $rules;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function messages()
    {
        return [
            'register_fullname.required' => 'Bạn phải điền vào trường này',
            'register_fullname.string' => 'Bạn phải điền kí tự vào trường này',
            'register_fullname.max' => 'Bạn chỉ được nhập tối đa 50 kí tự',
            'register_username.min' => 'Bạn phải nhập tối thiểu 6 kí tự',
            'register_username.required' => 'Bạn phải điền vào trường này',
            'register_username.max' => 'Bạn chỉ được nhập tối đa 50 kí tự',
            'register_username.unique' => 'Tài khoản đã tồn tại. Vui lòng chọn tài khoản khác',
            'register_password.required' => 'Bạn phải điền vào trường này',
            'register_password.min' => 'Bạn phải nhập tối thiểu 8 kí tự',
            'register_password.regex' => 'Bạn phải dùng password có chữ hoa & kí hiệu',
            'register_confirm_password.required' => 'Bạn phải điền vào trường này',
            'register_confirm_password.same' => 'Bạn phải xác nhận đúng mật khẩu',
            'register_email.required' => 'Bạn phải điền vào trường này',
            'register_email.email' => 'Bạn phải nhập theo định dạng gmail',
            'register_email.max' => 'Bạn chỉ được nhập tối đa 100 kí tự',
            'register_email.unique' => 'Email đã tồn tại. Vui lòng chọn email khác',
            'register_phone.required' => 'Bạn phải điền vào trường này',
            'register_phone.unique' => 'Số điện thoại đã tồn tại. Vui lòng chọn số điện thoại khác',
        ];
    }
}

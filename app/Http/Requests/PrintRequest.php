<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PrintRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'chieu_ngang' => 'required|numeric|min:1',
            'chieu_doc' => 'required|numeric|min:1',
            'so_luong' => 'required|numeric|min:1',
            'so_mat_in' => 'required',
            'kho_may_option' => 'required',
            'loai_giay_option' => 'required',
            // 'kho_giay_option' => 'required'
        ];
        switch ($this->request->get('kho_may_option')) {
            case '360x520':
                $rules['chieu_ngang'] = 'required|numeric|min:1|lte:360';
                $rules['chieu_doc'] = 'required|numeric|min:1|lte:520';
                break;
            case '520x720':
                $rules['chieu_ngang'] = 'required|numeric|min:1|lte:520';
                $rules['chieu_doc'] = 'required|numeric|min:1|lte:720';
                break;
            case '790x1090':
                $rules['chieu_ngang'] = 'required|numeric|min:1|lte:790';
                $rules['chieu_doc'] = 'required|numeric|min:1|lte:1090';
                break;
            case 'optionXY':
                $rules['X_input'] = 'required|numeric|min:1';
                $rules['Y_input'] = 'required|numeric|min:1';
                // $rules['P_input'] = 'required|numeric|min:1';
                // $rules['T_input'] = 'required|numeric|min:1';
                break;
            default:
                break;
        }
        switch ($this->request->get('loai_giay_option')) {
            case 'optionO':
                $rules['dinh_luong_giay_input'] = 'required|numeric|min:1';
                $rules['gia_giay_input'] = 'required|numeric|min:1';
                break;
            default:
                break;
        }
        // switch ($this->request->get('kho_giay_option')) {
        //     case 'optionU':
        //         $rules['kho_giay_1_input'] = 'required|numeric|min:1';
        //         $rules['kho_giay_2_input'] = 'required|numeric|min:1';
        //         break;
        //     default:
        //         break;
        // }
        return $rules;
    }
    
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function messages()
    {
        return [
            'chieu_ngang.lte' => 'Chiều ngang phải nhỏ hơn khổ máy X',
            'chieu_doc.lte' => 'Chiều dọc phải nhỏ hơn khổ máy Y',
        ];
    }
}

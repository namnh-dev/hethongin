<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'modalEditUserFullname' => 'required|string|max:50',
            'modalEditEmail' => 'required|email|max:100|unique:user,email,' . auth()->id(),
            'modalEditUserGender' => 'required|in:0,1',
            'modalEditUserDob' => 'required|date|before:today',
            'modalEditUserAddress' => 'required|max:255',
            'modalEditUserCity' => 'required',
            'modalEditUserProvince' => 'required',
            'modalEditUserWard' => 'required'
        ];
        return $rules;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function messages()
    {
        return [
            'modalEditUserFullname.required' => 'Bạn phải điền vào trường này',
            'modalEditUserFullname.string' => 'Bạn phải điền kí tự vào trường này',
            'modalEditUserFullname.max' => 'Bạn chỉ được nhập tối đa 50 kí tự',
            'modalEditEmail.required' => 'Bạn phải điền vào trường này',
            'modalEditEmail.email' => 'Bạn phải nhập theo định dạng gmail',
            'modalEditEmail.max' => 'Bạn chỉ được nhập tối đa 100 kí tự',
            'modalEditEmail.unique' => 'Email đã tồn tại. Vui lòng chọn email khác',
            'modalEditUserGender.required' => 'Bạn phải điền vào trường này',
            'modalEditUserDob.required' => 'Bạn phải điền vào trường này',
            'modalEditUserDob.date' => 'Bạn phải điền dạng datetime vào đây',
            'modalEditUserDob.before' => 'Bạn không được nhập ngày sau hôm nay',
            'modalEditUserAddress.required' => 'Bạn phải điền vào trường này',
            'modalEditUserAddress.max' => 'Bạn chỉ được nhập tối đa 255 kí tự',
            'modalEditUserCity.required' => 'Hãy chọn thành phố của bạn',
            'modalEditUserProvince.required' => 'Hãy chọn quận/ huyện của bạn',
            'modalEditUserWard.required' => 'Hãy chọn xã/ phường của bạn',
        ];
    }
}

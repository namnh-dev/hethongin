<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ChangePassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'oldPassword' => [
                'required',
                function ($attribute, $value, $fail) {
                    if (!Auth::attempt(['email' => $this->user()->email, 'password' => $value])) {
                        $fail('Vui lòng nhập đúng mật khẩu cũ !!');
                    }
                },
            ],
            'newPassword' => [
                'required',
                'min:8',
                'regex:/^(?=.*[A-Z])(?=.*[\W])/',
                function ($attribute, $value, $fail) {
                    if ($value === $this->input('oldPassword')) {
                        $fail('Mật khẩu mới phải khác mật khẩu cũ !!');
                    }
                },
            ],
        ];
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function messages()
    {
        return [
            'oldPassword.required' => 'Bạn phải điền vào trường này',
            'newPassword.required' => 'Bạn phải điền vào trường này',
            'newPassword.min' => 'Bạn phải nhập tối thiểu 8 kí tự',
            'newPassword.regex' => 'Bạn phải dùng mật khẩu có chữ hoa & kí hiệu'
        ];
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\PrintRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CalculateController extends Controller
{
    // calculate print
    public function calculate_print()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('content/calculate/calculate-print', ['pageConfigs' => $pageConfigs]);
    }

    public function printing(PrintRequest $request)
    {
        print($request);
        $request->except('_token');
        $json_gia_giay_file = file_get_contents(resource_path('data/bang-gia-giay.json'));
        
        global $data_gia_giay;
        $data_gia_giay = json_decode($json_gia_giay_file, true);

        /* Check loại giấy */
        // global $printMachineType;
        // global $printMachineTypeCost;
        // global $paperPenatyCost;

        // switch ($request->kho_may_option) {
        //     case '360x520':
        //         $printerX = 360;
        //         $printerY = 520;
        //         $printMachineType = '360x520';
        //         $printMachineTypeCost = 480000;
        //         $paperPenatyCost = 100;
        //         break;
        //     case '520x720':
        //         $printerX = 520;
        //         $printerY = 720;
        //         $printMachineType = '520x720';     
        //         $printMachineTypeCost = 800000;           
        //         $paperPenatyCost = 150;
        //         break;
        //     case '790x1090':
        //         $printerX = 790;
        //         $printerY = 1090;
        //         $printMachineType = '790x1090';
        //         $printMachineTypeCost = 1000000;
        //         $paperPenatyCost = 200;
        //         break;

        //     case 'optionXY':
        //         $printerX = $request->X_input;
        //         $printerY = $request->Y_input;
        //         $printMachineType = $printerX . 'x' . $printerY;
        //         $printMachineTypeCost = 500000;
        //         $paperPenatyCost = 200;  
        //         break;
        // }


        global $printA;
        global $printB;
        $printA = $request->chieu_ngang;
        $printB = $request->chieu_doc;
        global $printType;
        $printType = $request->so_mat_in;
        global $printAmount;
        $printAmount = $request->so_luong;

        // Giá giấy
        global $loai_giay;
        global $dinh_luong_giay;
        if ($request->loai_giay_option == "optionO") {
            $gia_giay = $request->gia_giay_input;
        } else {
            $loai_giay = $request->loai_giay_option;
            $dinh_luong_giay = $request->dinh_luong_giay_option;
        }

        // Lấy 3 lời giải tương ứng
        $solutionOncase_1 = $this->get_solution_on_case(360, 520, $printA, $printB, $printType, $printAmount);
        $solutionOncase_2 = $this->get_solution_on_case(520, 720, $printA, $printB, $printType, $printAmount);
        $solutionOncase_3 = $this->get_solution_on_case(790, 1090, $printA, $printB, $printType, $printAmount);
        $allSolution = [$solutionOncase_1, $solutionOncase_2, $solutionOncase_3];
        Session::flash('allSolution', $allSolution);

        return redirect()->back()->withInput();

    }

    public function get_solution_on_case($printerX, $printerY, $printA, $printB, $printType, $printAmount) {
        $solution_best = $this->solution($printerX, $printerY, $printA, $printB, $printType);
        $solution_easy = $this->solution_easy($printerX, $printerY, $printA, $printB, $printType);
        
        list($solution_1, $solution_3) = $solution_best;
        list($solution_2, $solution_4) = $solution_easy;

        $solutionHard_1 = array(
            'sizeCrop'=>$solution_1['sizeCrop'][0] . 'x' .$solution_1['sizeCrop'][1],
            'countPiece'=>$solution_1['countPiece'],
            'khogiay'=> $solution_1['khogiay'][0] . 'x' . $solution_1['khogiay'][1],
            'count'=> $solution_1['count'],
            'totalCost'=>$solution_1['totalCost'],
            'machineCost'=> $solution_1['machineCost'],
            'paperCost'=>$solution_1['paperCost'],
            'countPaper'=>$solution_1['countPaper'],
            'gia_giay'=>$solution_1['gia_giay'],
            'base64image_1'=>$solution_1['b64_1'],
            'base64image_2'=> $solution_1['b64_2']
        );

        $solutionEasy_2 = array(
            'sizeCrop'=>$solution_2['sizeCrop'][0] . 'x' .$solution_2['sizeCrop'][1],
            'countPiece'=>$solution_2['countPiece'],
            'khogiay'=> $solution_2['khogiay'][0] . 'x' . $solution_2['khogiay'][1],
            'count'=> $solution_2['count'],
            'totalCost'=>$solution_2['totalCost'],
            'machineCost'=> $solution_2['machineCost'],
            'paperCost'=>$solution_2['paperCost'],
            'countPaper'=>$solution_2['countPaper'],
            'gia_giay'=>$solution_2['gia_giay'],
            'base64image_1'=>$solution_2['b64_1'],
            'base64image_2'=> $solution_2['b64_2']
        );

        $solutionHard_3 = array(
            'sizeCrop'=>$solution_3['sizeCrop'][0] . 'x' .$solution_3['sizeCrop'][1],
            'countPiece'=>$solution_3['countPiece'],
            'khogiay'=> $solution_3['khogiay'][0] . 'x' . $solution_3['khogiay'][1],
            'count'=> $solution_3['count'],
            'totalCost'=>$solution_3['totalCost'],
            'machineCost'=> $solution_3['machineCost'],
            'paperCost'=>$solution_3['paperCost'],
            'countPaper'=>$solution_3['countPaper'],
            'gia_giay'=>$solution_3['gia_giay'],
            'base64image_1'=>$solution_3['b64_1'],
            'base64image_2'=> $solution_3['b64_2']
        );

        $solutionEasy_4 = array(
            'sizeCrop'=>$solution_4['sizeCrop'][0] . 'x' .$solution_4['sizeCrop'][1],
            'countPiece'=>$solution_4['countPiece'],
            'khogiay'=> $solution_4['khogiay'][0] . 'x' . $solution_4['khogiay'][1],
            'count'=> $solution_4['count'],
            'totalCost'=>$solution_4['totalCost'],
            'machineCost'=> $solution_4['machineCost'],
            'paperCost'=>$solution_4['paperCost'],
            'countPaper'=>$solution_4['countPaper'],
            'gia_giay'=>$solution_4['gia_giay'],
            'base64image_1'=>$solution_4['b64_1'],
            'base64image_2'=> $solution_4['b64_2']
        );

        // $solutionHard = $solutionEasy; // Dumy
        $baseInfo = array(
            'machineType' => 'Máy ' . $printerX . 'x' . $printerY,
            'paperSize'=>$printA . 'x' . $printB,
            'paperAmount'=>$printAmount,
            'printType'=>$printType,
            'sizeMachine'=>$printerX . 'x' . $printerY
        );

        $solutionOncase = array(
            'baseInfo'=>$baseInfo,
            'solution_1'=>$solutionHard_1,
            'solution_2'=> $solutionEasy_2,
            'solution_3'=>$solutionHard_3,
            'solution_4'=> $solutionEasy_4,            
        );

        return $solutionOncase;

    }

    public function get_machine_cost($W, $H){
        if ($W == 360 && $H == 520) return [480000, 100];
        if ($W == 520 && $H == 720) return [800000, 150];
        if ($W == 790 && $H == 1090) return [1000000, 200];
    }
    public function can_fit_in_box($piece_size, $box_size) {
        list($w, $h) = $piece_size;
        list($A, $B) = $box_size;
    
        // Kiểm tra xem mảnh giấy có vừa với hộp theo cả hai chiều hay không
        // Nếu mảnh giấy có thể vừa với hộp theo chiều nào đó, trả về true
        return ($w <= $A && $h <= $B) || ($w <= $B && $h <= $A);
    }
    
    public function find_cut_dimensions($W, $H, $N, $opt = 0) {
        // Nếu opt bằng 1, đổi giá trị của W và H
        if ($opt == 1) {
            list($W, $H) = array($H, $W);
        }

        // Nếu N chia hết cho 2, trả về kích thước tương ứng
        if ($N % 2 == 0 && $N > 2) {
            return array($W / ($N / 2), $H / ($N / 2), floor($N*$N/4));
        }

        // Trả về kích thước khác nếu N không chia hết cho 2
        return array($W / $N, $H, $N);
    }

    public function solve($W, $H, $w, $h, $a, $b, $typePrint){
        // Lời giải cho từng case
        // Khổ máy: W, H
        // Mảnh cắt: w, h
        // Khổ giấy: a, b

        // Lặp qua từng N trong (1, 2, 3, N)
        $countBest = 1;
        $nBest = 0;
        $sizeBest = [$W, $H];
        $cutBest = [];
        $cBest = 1;

        for ($N = 1; $N <= 6; $N++) {
            for ($opt = 0; $opt <=1; $opt ++){
                // Tìm các kích thước mảnh cắt nếu có 
                $cut_dimensions = $this->find_cut_dimensions($a, $b, $N, $opt);            
                // Nếu tìm thấy kích thước, kiểm tra xem có vừa với hộp không
                
                list($cut_w, $cut_h, $c) = $cut_dimensions;
                $piece_size = array($cut_w, $cut_h);
                $box_size = array($W, $H);

                // Sử dụng hàm can_fit_in_box để kiểm tra
                if ($this->can_fit_in_box($piece_size, $box_size)) {
                    
                    // Xếp giấy vào khung trên
                    $result = $this->calculatePaperCutting($cut_w, $cut_h, $w, $h, $typePrint);
                    $count = count($result['cuts'])*$c;

                    if ($count > $countBest) {
                        $cutBest = $result['cuts'];
                        $countBest = $count;
                        $nBest = $N;
                        $cBest = $c;
                        $sizeBest = [$result['W'], $result['H']];
                    }
                }
            }
        }

        // exit(0);
        $solution = array(
            'size' => $sizeBest,
            'N' => $nBest,
            'count' => $cBest,
            'totalCount' => count($cutBest),
            'cuts' => $cutBest
        );

        return $solution;
    }
    
    public function solution($W, $H, $w, $h, $typePrint) {
        global $printAmount;

        list($printMachineTypeCost, $paperPenatyCost) = $this->get_machine_cost($W, $H);
        // Case 1: Khổ 650x860
        $wCase1 = 650;
        $hCase1 = 860;
        // Lời giải case 1
        $solutionCase1 = $this->solve($W, $H, $w, $h, $wCase1, $hCase1, $typePrint);
        $costCase1 = $this->getPaperPrice('650x860');

        // Case 2: Khổi 790x1090
        $wCase2 = 790;
        $hCase2 = 1090;        
        // Lời giải case 2
        $solutionCase2 = $this->solve($W, $H, $w, $h, $wCase2, $hCase2, $typePrint);
        $costCase2 = $this->getPaperPrice('790x1090');

        // Vẽ giấy trên khổ cắt
        $data1 = $this->calculatePaperCutting($solutionCase1['size'][0], $solutionCase1['size'][1], $w, $h, $typePrint);
        $data_case1 = $this->createRectangleImage($data1['cuts'], $data1['W'], $data1['H'], 'data/output_3.jpg', true, false, 2);
        $data2 = $this->calculatePaperCutting($solutionCase2['size'][0], $solutionCase2['size'][1], $w, $h, $typePrint);
        $data_case2 = $this->createRectangleImage($data2['cuts'], $data2['W'], $data2['H'], 'data/output_4.jpg', true, false, 2);
        
        // Xếp khổ cắt vào khổ giấy
        $output1 = $this->calculatePaperCutting($wCase1, $hCase1, $solutionCase1['size'][0], $solutionCase1['size'][1], $typePrint, 0);
        $output_case1 = $this->createRectangleImage($output1['cuts'], $output1['W'], $output1['H'], 'data/output_3.jpg', true, false);
        
        $output2 = $this->calculatePaperCutting($wCase2, $hCase2, $solutionCase2['size'][0], $solutionCase2['size'][1], $typePrint, 0);
        $output_case2 = $this->createRectangleImage($output2['cuts'], $output2['W'], $output2['H'], 'data/output_4.jpg', true, false);

        // echo "<pre>";
        // print_r([$wCase1, $hCase1]);
        // print_r($output1);
        // print_r([$solutionCase1['size'][0], $solutionCase1['size'][1]]);
        // echo "</pre>";

        $totalCostCase1 = ceil($printAmount / (count($data1['cuts'])* count($output1['cuts']))) * $costCase1 * $typePrint;
        $totalCostCase2 = ceil($printAmount / (count($data2['cuts'])*count($output2['cuts']))) * $costCase2 * $typePrint;
        // Machine Cost
        $penatyCost = 0;
        if ($printAmount*$typePrint > 1000)
            $penatyCost = ($printAmount*$typePrint-1000)*$paperPenatyCost;

            $totalCostFull1 =  $printMachineTypeCost + $penatyCost + $totalCostCase1;
            
            $solutionA = array(
                        'khogiay'=> [$wCase1, $hCase1],
                        'sizeCrop' => [ceil($solutionCase1['size'][0]), ceil($solutionCase1['size'][1])],
                        'solution' => $solutionCase1,
                        'count' => count($output1['cuts']),
                        'countPiece' => count($data1['cuts']),
                        'countPaper' => ceil($printAmount / (count($data1['cuts'])*count($output1['cuts']))),
                        'gia_giay' => $costCase1,
                        'b64_1' => $data_case1['b64'],
                        'b64_2' => $output_case1['b64'],
                        'totalCost' => $totalCostFull1,
                        'paperCost' => $totalCostCase1,
                        'machineCost' =>  $printMachineTypeCost + $penatyCost,
                ); 

            $totalCostFull2 =  $printMachineTypeCost + $penatyCost + $totalCostCase2;
            $solutionB =  array(
                'khogiay'=> [$wCase2, $hCase2],
                'sizeCrop' => [ceil($solutionCase2['size'][0]), ceil($solutionCase2['size'][1])],
                'solution' => $solutionCase2,
                'count' => count($output2['cuts']),
                'countPiece' => count($data2['cuts']),
                'countPaper' => ceil($printAmount / (count($data2['cuts'])*count($output2['cuts']))),
                'gia_giay' => $costCase2,
                'b64_1' => $data_case2['b64'],
                'b64_2' => $output_case2['b64'],
                'totalCost' => $totalCostFull2,
                'paperCost' => $totalCostCase2,
                'machineCost' =>  $printMachineTypeCost + $penatyCost,                

            );

            return [$solutionA, $solutionB];
        }

    public function solve_easy($W, $H, $w, $h, $a, $b, $typePrint){
        // Lời giải cho từng case
        // Khổ máy: W, H
        // Mảnh cắt: w, h
        // Khổ giấy: a, b

        // Lặp qua từng N trong (1, 2, 3, N)
        $countBest = 1;
        $nBest = 0;
        $sizeBest = [$W, $H];
        $cutBest = [];
        $cBest = 1;

        for ($N = 1; $N <= 6; $N++) {
            for ($opt = 0; $opt <=1; $opt ++){
                // Tìm các kích thước mảnh cắt nếu có 
                $cut_dimensions = $this->find_cut_dimensions($a, $b, $N, $opt);            
                // Nếu tìm thấy kích thước, kiểm tra xem có vừa với hộp không
                
                list($cut_w, $cut_h, $c) = $cut_dimensions;
                $piece_size = array($cut_w, $cut_h);
                $box_size = array($W, $H);

                // Sử dụng hàm can_fit_in_box để kiểm tra
                if ($this->can_fit_in_box($piece_size, $box_size)) {
                    // Xếp giấy vào khung trên
                    $result = $this->calculatePaperCutting_easy($cut_w, $cut_h, $w, $h, $typePrint);
                    $count = count($result['cuts'])*$c;
                    if ($count > $countBest) {
                        $cutBest = $result['cuts'];
                        $countBest = $count;
                        $nBest = $N;
                        $cBest = $c;
                        $sizeBest = [$result['W'], $result['H']];
                    }
                }
            }
        }

        $solution = array(
            'size' => $sizeBest,
            'N' => $nBest,
            'count' => $cBest,
            'totalCount' => count($cutBest),
            'cuts' => $cutBest
        );

        return $solution;
    }

    public function solution_easy($W, $H, $w, $h, $typePrint) {
        global $printAmount;

        list($printMachineTypeCost, $paperPenatyCost) = $this->get_machine_cost($W, $H);

        // Case 1: Khổ 650x860
        $wCase1 = 650;
        $hCase1 = 860;
        // Lời giải case 1
        $solutionCase1 = $this->solve_easy($W, $H, $w, $h, $wCase1, $hCase1, $typePrint);
        $costCase1 = $this->getPaperPrice('650x860');
        // if ($solutionCase1['totalCount'] == 0)
        //     $totalCostCase1 = 1000000000;
        // else
        
        // Case 2: Khổi 790x1090
        $wCase2 = 790;
        $hCase2 = 1090;        
        // Lời giải case 2
        $solutionCase2 = $this->solve_easy($W, $H, $w, $h, $wCase2, $hCase2, $typePrint);
        $costCase2 = $this->getPaperPrice('790x1090');
        
        // if ($solutionCase2['totalCount'] == 0)
        //     $totalCostCase2 = 1000000000;
        // else
        
        // Vẽ giấy trên khổ cắt
        $data1 = $this->calculatePaperCutting_easy($solutionCase1['size'][0], $solutionCase1['size'][1], $w, $h, $typePrint);
        $data_case1 = $this->createRectangleImage($data1['cuts'], $data1['W'], $data1['H'], 'data/output_3.jpg', true, false, 2);
        $data2 = $this->calculatePaperCutting_easy($solutionCase2['size'][0], $solutionCase2['size'][1], $w, $h, $typePrint);
        $data_case2 = $this->createRectangleImage($data2['cuts'], $data2['W'], $data2['H'], 'data/output_4.jpg', true, false, 2);
        
        // Xếp khổ cắt vào khổ giấy
        $output1 = $this->calculatePaperCutting($wCase1, $hCase1, $solutionCase1['size'][0], $solutionCase1['size'][1], $typePrint, 0);
        $output_case1 = $this->createRectangleImage($output1['cuts'], $output1['W'], $output1['H'], 'data/output_3.jpg', true, false);
        
        $output2 = $this->calculatePaperCutting($wCase2, $hCase2, $solutionCase2['size'][0], $solutionCase2['size'][1], $typePrint, 0);
        $output_case2 = $this->createRectangleImage($output2['cuts'], $output2['W'], $output2['H'], 'data/output_4.jpg', true, false);
        
        
        $totalCostCase1 = ceil($printAmount / (count($data1['cuts'])*count($output1['cuts']))) * $costCase1 * $typePrint;
        $totalCostCase2 = ceil($printAmount / (count($data2['cuts'])*count($output2['cuts']))) * $costCase2 * $typePrint;
        
        
        // Machine Cost
        $penatyCost = 0;
        if ($printAmount*$typePrint > 1000)
            $penatyCost = ($printAmount*$typePrint-1000)*$paperPenatyCost;

                $totalCostFull1 =  $printMachineTypeCost + $penatyCost + $totalCostCase1;
            
                $solutionA = array(
                            'khogiay'=> [$wCase1, $hCase1],
                            'sizeCrop' => [ceil($solutionCase1['size'][0]), ceil($solutionCase1['size'][1])],
                            'solution' => $solutionCase1,
                            'count' => count($output1['cuts']),
                            'countPiece' => count($data1['cuts']),
                            'countPaper' => ceil($printAmount / (count($data1['cuts'])* count($output1['cuts']))),
                            'gia_giay' => $costCase1,
                            'b64_1' => $data_case1['b64'],
                            'b64_2' => $output_case1['b64'],
                            'totalCost' => $totalCostFull1,
                            'paperCost' => $totalCostCase1,
                            'machineCost' =>  $printMachineTypeCost + $penatyCost,
                    ); 
    
                $totalCostFull2 =  $printMachineTypeCost + $penatyCost + $totalCostCase2;
                $solutionB =  array(
                    'khogiay'=> [$wCase2, $hCase2],
                    'sizeCrop' => [ceil($solutionCase2['size'][0]), ceil($solutionCase2['size'][1])],
                    'solution' => $solutionCase2,
                    'count' => count($output2['cuts']),
                    'countPiece' => count($data2['cuts']),
                    'countPaper' => ceil($printAmount / (count($data2['cuts'])*count($output2['cuts']))),
                    'gia_giay' => $costCase2,
                    'b64_1' => $data_case2['b64'],
                    'b64_2' => $output_case2['b64'],
                    'totalCost' => $totalCostFull2,
                    'paperCost' => $totalCostCase2,
                    'machineCost' =>  $printMachineTypeCost + $penatyCost,                
    
                );
    
                return [$solutionA, $solutionB];
        }

    public function getPaperPrice($kho_giay) {
        global $loai_giay;
        global $dinh_luong_giay;
        global $data_gia_giay;

        $gia_giay = 1;
        foreach ($data_gia_giay as $object) {
            if ($object['loai_giay'] == $loai_giay && $object['kho_giay'] == $kho_giay && $object['dinh_luong'] == $dinh_luong_giay) {
                $gia_giay = $object['don_gia'];
            }
        }
        return $gia_giay;
    }
    

    public function find_min_remainder($N, $M, $a, $b) {
        $min_remainder = INF;
        $best_x = $best_y = 0;

        if ($a == 0)return array($best_x, $best_y, $min_remainder);
        if ($b == 0)return array($best_x, $best_y, $min_remainder);

        for ($x = 0; $x <= floor($N / $a); $x++) {
            if ($x && $M < $b) continue;
            for ($y = 0; $y <= floor($N / $b); $y++) {
                if ($y && $M < $a) continue;
                $total = $x * $a + $y * $b;
                if ($total <= $N) {
                    $remainder = $N - $total;
                    if ($remainder < $min_remainder) {
                        $min_remainder = $remainder;
                        $best_x = $x;
                        $best_y = $y;
                    }
                }
            }
        }

        return array($best_x, $best_y, $min_remainder);
    }

    public function find_min_remainder_easy($N, $M, $a, $b) {
        $min_remainder = INF;
        $best_x = $best_y = 0;

        if ($a == 0)return array($best_x, $best_y, $min_remainder);
        if ($b == 0)return array($best_x, $best_y, $min_remainder);

        // Case1:
        $c1 = floor($N / $a) * floor($M / $b);
        $c2 = floor($M / $a) * floor($N / $b);

        if ($c1 > $c2)
            return array(floor($N/$a), 0, -1);

        return array(0, floor($N/$b), -1);
    }

    //
    public function calculatePaperCutting($W, $H, $w, $h, $printType, $opt=1) {
        $size1 = [$w, $h];
        $size2 = [$h, $w];
        // Gọi hàm optimalCuts với hai trường hợp kích thước khác nhau
        list($cuts1, $totalCuts1) = $this->optimalCuts($W, $H, $size1, $size2);
        list($cuts2, $totalCuts2) = $this->optimalCuts($H, $W, $size1, $size2);

        // So sánh và chọn kết quả tối ưu
        if (count($cuts1) > count($cuts2)) {
            $cuts = $cuts1;
            $totalCuts = $totalCuts1;
        } elseif (count($cuts1) == count($cuts2)) {
            if ($totalCuts1 < $totalCuts2) {
                $cuts = $cuts1;
                $totalCuts = $totalCuts1;
            } else {
                $cuts = $cuts2;
                $totalCuts = $totalCuts2;
                list($W, $H) = [$H, $W]; // Đổi kích thước W và H nếu cần
            }
        } else {
            $cuts = $cuts2;
            $totalCuts = $totalCuts2;
            list($W, $H) = [$H, $W]; // Đổi kích thước W và H nếu cần
        }
        // Tính toán diện tích sử dụng
        $areaUsed = count($cuts) * $size1[0] * $size1[1] / ($W * $H);
        

        // exit(0);

        // Đếm loại ngang/dọc
        $countType1 = 0;
        $countType2 = 0;
        foreach ($cuts as $item) {
            if ($item['type'] == 1) {
                $countType1++;
            } elseif ($item['type'] == 2) {
                $countType2++;
            }
        }

        // Xử lý cho loại 2 mặt
        if($opt == 1 && $printType == 2){
            if(count($cuts) % 2 != 0){
                array_pop($cuts);
            }
        }

        // Kết thúc hàm và trả về kết quả
        return array(
            'cuts' => $cuts,
            'areaUsed' => $areaUsed,
            'totalCuts' => $totalCuts,
            'W' => $W,
            'H' => $H,
        );        
    }
    
    public function calculatePaperCutting_easy($W, $H, $w, $h, $printType) {
        $size1 = [$w, $h];
        $size2 = [$h, $w];
        // Gọi hàm optimalCuts với hai trường hợp kích thước khác nhau
        list($cuts1, $totalCuts1) = $this->optimalCuts_easy($W, $H, $size1, $size2);
        list($cuts2, $totalCuts2) = $this->optimalCuts_easy($H, $W, $size1, $size2);

        // So sánh và chọn kết quả tối ưu
        if (count($cuts1) > count($cuts2)) {
            $cuts = $cuts1;
            $totalCuts = $totalCuts1;
        } elseif (count($cuts1) == count($cuts2)) {
            if ($totalCuts1 < $totalCuts2) {
                $cuts = $cuts1;
                $totalCuts = $totalCuts1;
            } else {
                $cuts = $cuts2;
                $totalCuts = $totalCuts2;
                list($W, $H) = [$H, $W]; // Đổi kích thước W và H nếu cần
            }
        } else {
            $cuts = $cuts2;
            $totalCuts = $totalCuts2;
            list($W, $H) = [$H, $W]; // Đổi kích thước W và H nếu cần
        }

        // Tính toán diện tích sử dụng
        $areaUsed = count($cuts) * $size1[0] * $size1[1] / ($W * $H);
        
        // Đếm loại ngang/dọc
        $countType1 = 0;
        $countType2 = 0;
        foreach ($cuts as $item) {
            if ($item['type'] == 1) {
                $countType1++;
            } elseif ($item['type'] == 2) {
                $countType2++;
            }
        }

        // Xử lý cho loại 2 mặt
        if($printType == 2){
            if(count($cuts) % 2 != 0){
                array_pop($cuts);
            }
        }

        // Kết thúc hàm và trả về kết quả
        return array(
            'cuts' => $cuts,
            'areaUsed' => $areaUsed,
            'totalCuts' => $totalCuts,
            'W' => $W,
            'H' => $H,
        );        
    }

    public function optimalCuts($W, $H, $size1, $size2, $xOffset = 0, $yOffset = 0, $cuts = null, $totalCuts = 0) {
            if ($cuts === null) {
                $cuts = [];
            }
            if (!($size1[0] <= $H && $size1[1] <= $W)) {
                return [$cuts, $totalCuts];
            }
    
            list($a, $b, $_) = $this->find_min_remainder($W, $H, $size1[0], $size2[0]);


            if($a==0 && $b ==0) return [0, 0];

            $totalCuts += ($a > 0 && $b > 0) ? 3 : 1;
    
            $stacksSize1 = intdiv($H, $size1[1]);
            $stacksSize2 = intdiv($H, $size2[1]);
            for ($i = 0; $i < $a; $i++) {
                for ($j = 0; $j < $stacksSize1; $j++) {
                    $cuts[] = ['x' => $xOffset + $i * $size1[0], 'y' => $yOffset + $j * $size1[1], 'w' => $size1[0], 'h' => $size1[1], 'type' => 1];
                }
            }
            for ($i = 0; $i < $b; $i++) {
                for ($j = 0; $j < $stacksSize2; $j++) {
                    $cuts[] = ['x' => $xOffset + $a * $size1[0] + $i * $size2[0], 'y' => $yOffset + $j * $size2[1], 'w' => $size2[0], 'h' => $size2[1], 'type' => 2];
                }
            }
    
            $totalCuts += max(1, $a - 2) * max(1, $stacksSize1 - 2) + max(1, $b - 2) * max(1, $stacksSize2 - 2);

            // Xử lý remainders
            $remainders = [
                [0, intdiv($H, $size1[1]) * $size1[1], $a * $size1[0], $H],
                [$a * $size1[0], intdiv($H, $size2[1]) * $size2[1], $W, $H],
                [$a * $size1[0] + $b * $size2[0], 0, $W, intdiv($H, $size2[1]) * $size2[1]]
            ];

            foreach ($remainders as $remainder) {
                list($x1, $y1, $x2, $y2) = $remainder;
                list($newCuts, $newTotal) = $this->optimalCuts($x2 - $x1, $y2 - $y1, $size1, $size2, $x1, $y1, $cuts, $totalCuts);
                $cuts = $newCuts;
                $totalCuts = $newTotal;
            }

            return [$cuts, $totalCuts];
        }

    public function optimalCuts_easy($W, $H, $size1, $size2, $xOffset = 0, $yOffset = 0, $cuts = null, $totalCuts = 0) {
            if ($cuts === null) {
                $cuts = [];
            }
            if (!($size1[0] <= $H && $size1[1] <= $W)) {
                return [$cuts, $totalCuts];
            }
    
            list($a, $b, $_) = $this->find_min_remainder_easy($W, $H, $size1[0], $size2[0]);
            if($a==0 && $b ==0) return [[], 0];

            $totalCuts += ($a > 0 && $b > 0) ? 3 : 1;
    
            $stacksSize1 = intdiv($H, $size1[1]);
            $stacksSize2 = intdiv($H, $size2[1]);
            for ($i = 0; $i < $a; $i++) {
                for ($j = 0; $j < $stacksSize1; $j++) {
                    $cuts[] = ['x' => $xOffset + $i * $size1[0], 'y' => $yOffset + $j * $size1[1], 'w' => $size1[0], 'h' => $size1[1], 'type' => 1];
                }
            }
            for ($i = 0; $i < $b; $i++) {
                for ($j = 0; $j < $stacksSize2; $j++) {
                    $cuts[] = ['x' => $xOffset + $a * $size1[0] + $i * $size2[0], 'y' => $yOffset + $j * $size2[1], 'w' => $size2[0], 'h' => $size2[1], 'type' => 2];
                }
            }
    
            $totalCuts += max(1, $a - 2) * max(1, $stacksSize1 - 2) + max(1, $b - 2) * max(1, $stacksSize2 - 2);
            return [$cuts, $totalCuts];
        }

    public function calculatePaperCutting_stage2($W, $H, $w, $h, $printType) {
            $size1 = [$w, $h];
            $size2 = [$h, $w];
            // Gọi hàm optimalCuts với hai trường hợp kích thước khác nhau
            list($cuts1, $totalCuts1, $totalCount1) = $this->optimalCuts_stage2($W, $H, $size1, $size2);
            list($cuts2, $totalCuts2, $totalCount2) = $this->optimalCuts_stage2($H, $W, $size1, $size2);
    
            // So sánh và chọn kết quả tối ưu
            if (count($cuts1) > count($cuts2)) {
                $cuts = $cuts1;
                $totalCuts = $totalCuts1;
                $totalCount = $totalCount1;
            } elseif ($totalCount1 == $totalCount2) {
                if ($totalCuts1 < $totalCuts2) {
                    $cuts = $cuts1;
                    $totalCuts = $totalCuts1;
                    $totalCount = $totalCount1;
                } else {
                    $cuts = $cuts2;
                    $totalCuts = $totalCuts2;
                    $totalCount = $totalCount2;

                    list($W, $H) = [$H, $W]; // Đổi kích thước W và H nếu cần
                }
            } else {
                $cuts = $cuts2;
                $totalCuts = $totalCuts2;
                $totalCount = $totalCount2;
                list($W, $H) = [$H, $W]; // Đổi kích thước W và H nếu cần
            }
    
            // Tính toán diện tích sử dụng
            $areaUsed = $totalCount * $size1[0] * $size1[1] / ($W * $H);
            
            // Kết thúc hàm và trả về kết quả
            return array(
                'counts' => $totalCount,
                'cuts' => $cuts,
                'areaUsed' => $areaUsed,
                'totalCuts' => $totalCuts,
                'W' => $W,
                'H' => $H,
            );        
        }
    public function optimalCuts_stage2($W, $H, $size1, $size2, $xOffset = 0, $yOffset = 0, $cuts = null, $totalCuts = 0, $totalCount=0, $typeCount=1) {
            // Đếm số lượng box to trước
            global $printA;
            global $printB;

            if ($cuts === null) {
                $cuts = [];
            }
            if (!($size1[0] < $H && $size1[1] < $W)) {
                return [$cuts, $totalCuts, $totalCount];
            }
    
            list($a, $b, $_) = $this->find_min_remainder($W, $H, $size1[0], $size2[0]);
            if($a==0 && $b==0) return [0, 0];

            $totalCuts += ($a > 0 && $b > 0) ? 3 : 1;
            
            $stacksSize1 = intdiv($H, $size1[1]);
            $stacksSize2 = intdiv($H, $size2[1]);

            // Đếm tổng số lượng
            if ($typeCount == 1)
                $totalCount += floor(($W*$H) / ($size1[0]*$size1[1])) * floor(($size1[0]*$size1[1]) / ($printA*$printB));

            if ($typeCount == 2)
                $totalCount += ($a*$stacksSize1 + $b*$stacksSize2);

            for ($i = 0; $i < $a; $i++) {
                for ($j = 0; $j < $stacksSize1; $j++) {
                    $colorType = 1;
                    if ($typeCount == 1) $colorType = 3;
                    $cuts[] = ['x' => $xOffset + $i * $size1[0], 'y' => $yOffset + $j * $size1[1], 'w' => $size1[0], 'h' => $size1[1], 'type' => $colorType];
                }
            }
            for ($i = 0; $i < $b; $i++) {
                for ($j = 0; $j < $stacksSize2; $j++) {
                    $colorType = 2;
                    if ($typeCount == 1) $colorType = 3;
                    $cuts[] = ['x' => $xOffset + $a * $size1[0] + $i * $size2[0], 'y' => $yOffset + $j * $size2[1], 'w' => $size2[0], 'h' => $size2[1], 'type' => $colorType];
                }
            }
    
            $totalCuts += max(1, $a - 2) * max(1, $stacksSize1 - 2) + max(1, $b - 2) * max(1, $stacksSize2 - 2);
            
            // Xử lý remainders phần dư theo cách 2
            $remainders = [
                [0, intdiv($H, $size1[1]) * $size1[1], $a * $size1[0], $H],
                [$a * $size1[0], intdiv($H, $size2[1]) * $size2[1], $W, $H],
                [$a * $size1[0] + $b * $size2[0], 0, $W, intdiv($H, $size2[1]) * $size2[1]]
            ];

            global $printA;
            global $printB;
            $size1_new  = [$printA, $printB];
            $size2_new  = [$printB, $printA];
            foreach ($remainders as $remainder) {
                list($x1, $y1, $x2, $y2) = $remainder;
                list($newCuts, $newTotal, $newTotalCount) = $this->optimalCuts_stage2($x2 - $x1, $y2 - $y1, $size1_new, $size2_new, $x1, $y1, $cuts, $totalCuts, $totalCount, 2);
                $cuts = $newCuts;
                $totalCuts = $newTotal;
                $totalCount = $newTotalCount;
            }

            return [$cuts, $totalCuts, $totalCount];
        }
    public function createRectangleImage($rectangles, $width, $height, $outputFile, $rotation=true, $line=true, $colorType=1) {
        // Hàm vẽ hình 
        // Kiểm tra và kích hoạt thư viện GD
        if (!extension_loaded('gd')) {
            return false; // Thư viện GD không khả dụng
        }
    
        $image = imagecreatetruecolor($width, $height);
        $white = imagecolorallocate($image, 255, 255, 255);
        $black = imagecolorallocate($image, 0, 0, 0);
        $blue = imagecolorallocate($image, 50, 150, 255);
        $red = imagecolorallocate($image, 255, 90, 90);
        $green = imagecolorallocate($image, 0, 255, 90);
        $thickness = 5; // Ví dụ: độ dày là 5 pixels
        imagesetthickness($image, $thickness);
        imagefill($image, 0, 0, $white);
        $max_x = 0;
        $max_y = 0;
        foreach ($rectangles as $index => $rect) {
            // Vẽ hình chữ nhật với màu nền
            // if ($rect['type'] == 1)
            if ($colorType == 1)
            imagefilledrectangle($image, $rect['x'], $rect['y'], $rect['x'] + $rect['w'], $rect['y'] + $rect['h'], $red);
            if ($colorType == 2)
                imagefilledrectangle($image, $rect['x'], $rect['y'], $rect['x'] + $rect['w'], $rect['y'] + $rect['h'], $blue);
            // if($rect['type'] > 2)
                // imagefilledrectangle($image, $rect['x'], $rect['y'], $rect['x'] + $rect['w'], $rect['y'] + $rect['h'], $green);
            
            // Vẽ viền hình chữ nhật
            // if ($line)
            imagerectangle($image, $rect['x'], $rect['y'], $rect['x'] + $rect['w'], $rect['y'] + $rect['h'], $black);
            // imagestring($image, 5, $rect['x'] + $rect['w']/2, $rect['y'] + $rect['h']/2, ($index + 1), $black);
            if ($rect['x'] + $rect['w'] > $max_x) 
                if ($rotation) $max_x = $rect['x'] + $rect['h'];
                else $max_x = $rect['x'] + $rect['w'];

            if ($rect['y'] + $rect['h'] > $max_x) 
                if ($rotation) $max_y = $rect['y'] + $rect['w'];
                else $max_y = $rect['y'] + $rect['h'];

        }
        
            // Xoay hình ảnh
        if ($rotation)
            $image = imagerotate($image, 90, $white);


        imagepng($image, $outputFile);
        // Bắt đầu lưu output vào bộ nhớ đệm
        ob_start();
        imagepng($image);
        $contents = ob_get_contents();
        ob_end_clean();
    
        imagedestroy($image);
    
        return array(
        'maxX' => $max_x,
        'maxY' => $max_y,
        'b64' => base64_encode($contents)
        );
            
    }
}

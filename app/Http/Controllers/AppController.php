<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller
{
    // User Account Page
    public function user_view_account()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-view-account', ['pageConfigs' => $pageConfigs]);
    }
    // User Security Page
    public function user_view_security()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-view-security', ['pageConfigs' => $pageConfigs]);
    }
    // User Manage Pricing
    public function user_manage_pricing()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/pages/page-pricing', ['pageConfigs' => $pageConfigs]);
    }
    // User Account Factory
    public function user_manage_factory()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-view-factory', ['pageConfigs' => $pageConfigs]);
    }
    // Config Factory
    public function config_factory()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/pages/page-account-settings-account', ['pageConfigs' => $pageConfigs]);
    }
    // Config price
    public function config_price()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/forms/form-repeater', ['pageConfigs' => $pageConfigs]);
    }

    // Config price
    public function recharge_thueapi()
    {
        $config_url = config('services.thueapi.redirect');
        return response()->json(['success' => true, 'callback_url' => $config_url]);
    }

    // Config price
    public function callback_thueapi(Request $request)
    {
        $token = 'x6fbIOCnBw6rOglIQAvjzqvzQt5PyVBAllWJ2nSKsYNylfPuQB';
        dd($request->all());
    }
}

<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable as AuthenticatableTrait;

class Account extends Model implements Authenticatable
{
    use AuthenticatableTrait;
    public $timestamps = false; //set time to false
    protected $fillable = [
        'fullname', 'email', 'phone', 'username', 'password', 'gender', 'dob', 'address', 'city_id', 'province_id', 'ward_id', 'status'
    ];
    protected $primaryKey = 'id';
    protected $table = 'user';
}

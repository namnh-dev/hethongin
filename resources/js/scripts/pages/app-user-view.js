/*=========================================================================================
    File Name: app-user-view.js
    Description: User View page
    --------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
(function () {
    const suspendUser = document.querySelector('.suspend-user');
    // Suspend User javascript
    var moneyForm = $('#moneyForm');
    if (moneyForm.length) {
        moneyForm.validate({
            rules: {
                'moneyInput': {
                    required: true
                }
            }
        });
    }

    $(".120000").click(function () {
        $('#money_input').val(120000);
    });

    $(".300000").click(function () {
        $('#money_input').val(300000);
    });

    $(".500000").click(function () {
        $('#money_input').val(500000);
    });

    moneyForm.submit(function (event) {
        var _token = $('input[name="_token"]').val();
        event.preventDefault();
        $.ajax({
            url: "/recharge-thueapi",
            method: 'GET',
            dataType: 'json',
            data: {
                moneyIn: $('#money_input').val(),
                _token: _token
            },
            beforeSend: function(){
                $('#submitMoneyInForm').hide();
                $('#btnLoading').show();
            },
            success: function(data) {
                location.replace(data.callback_url);
            }
        });
    });

    if (suspendUser) {
        suspendUser.onclick = function () {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert user!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Suspend user!',
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-outline-danger ms-1'
                },
                buttonsStyling: false
            }).then(function (result) {
                if (result.value) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Suspended!',
                        text: 'User has been suspended.',
                        customClass: {
                            confirmButton: 'btn btn-success'
                        }
                    });
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire({
                        title: 'Cancelled',
                        text: 'Cancelled Suspension :)',
                        icon: 'error',
                        customClass: {
                            confirmButton: 'btn btn-success'
                        }
                    });
                }
            });
        };
    }

    //? Billing page have multiple buttons
    // Cancel Subscription alert
    const cancelSubscription = document.querySelectorAll('.cancel-subscription');

    // Alert With Functional Confirm Button
    if (cancelSubscription) {
        cancelSubscription.forEach(cancelBtn => {
            cancelBtn.onclick = function () {
                Swal.fire({
                    text: 'Are you sure you would like to cancel your subscription?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-outline-danger ms-1'
                    },
                    buttonsStyling: false
                }).then(function (result) {
                    if (result.value) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Unsubscribed!',
                            text: 'Your subscription cancelled successfully.',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        Swal.fire({
                            title: 'Cancelled',
                            text: 'Unsubscription Cancelled!!',
                            icon: 'error',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                    }
                });
            };
        });
    }

    const toggleButton = document.getElementById('toggle-button');
    const hiddenContent = document.getElementById('hidden-content');

    toggleButton.addEventListener('click', function () {
        if (hiddenContent.classList.contains('d-none')) {
            hiddenContent.classList.remove('d-none');
            hiddenContent.classList.add('d-block');
            toggleButton.textContent = 'Ẩn bớt';
        } else {
            hiddenContent.classList.remove('d-block');
            hiddenContent.classList.add('d-none');
            toggleButton.textContent = 'Xem thêm';
        }
    });

    $('.pickadate').pickadate({
        format: 'yyyy-mm-dd'
    });

})();
$(function () {
  const select2 = $('.select2'),
    editUserForm = $('#editUserForm'),
    modalEditUserPhone = $('.phone-number-mask');

  // Select2 Country
  if (select2.length) {
    select2.each(function () {
      var $this = $(this);
      $this.wrap('<div class="position-relative"></div>').select2({
        dropdownParent: $this.parent()
      });
    });
  }

  // Phone Number Input Mask
  if (modalEditUserPhone.length) {
    modalEditUserPhone.each(function () {
      new Cleave($(this), {
        phone: true,
        phoneRegionCode: 'US'
      });
    });
  }

  // Edit user form validation
  if (editUserForm.length) {
    editUserForm.validate({
      rules: {
        modalEditUserFirstName: {
          required: true
        },
        modalEditUserLastName: {
          required: true
        },
        modalEditUserName: {
          required: true,
          minlength: 6,
          maxlength: 30
        }
      },
      messages: {
        modalEditUserName: {
          required: 'Please enter your username',
          minlength: 'The name must be more than 6 and less than 30 characters long',
          maxlength: 'The name must be more than 6 and less than 30 characters long'
        }
      }
    });
  }

  let city_value = $("#city_id").text();
  let province_value = $("#province_id").text();
  let ward_value = $("#ward_id").text();

  $.getJSON('/data/dvhcvn.json', function (data) {
    const city = $("#modalEditUserCity");
    const province = $("#modalEditUserProvince");
    const ward = $("#modalEditUserWard");
    city.append($("<option>", {
      value: "",
      text: "Select city",
      selected: true,
      disabled: true
    }));
    data.forEach((option) => {
      var isSelected = (parseInt(option.city_id) == city_value);
      city.append($("<option>", {
        value: option.city_id,
        text: option.name,
        selected: isSelected
      }));
    });

    if (city_value) {
      province.empty();
      data.forEach((option) => {
        if (city_value == option.city_id) {
          var province_data = option.province;
          province.append($("<option>", {
            value: "",
            text: "Select province",
            selected: true,
            disabled: true
          }));
          province_data.forEach((option) => {
            var isSelected = (parseInt(option.province_id) == province_value);
            province.append($("<option>", {
              value: option.province_id,
              text: option.name,
              selected: isSelected
            }));
          });
        }
      });
    }

    city.change(function () {
      province.empty();
      data.forEach((option) => {
        if ($(this).val() == option.city_id) {
          var province_data = option.province;
          province.append($("<option>", {
            value: "",
            text: "Select province",
            selected: true,
            disabled: true
          }));
          province_data.forEach((option) => {
            province.append($("<option>", {
              value: option.province_id,
              text: option.name
            }));
          });
        }
      });
    });

    if (province_value) {
      ward.empty();
      data.forEach((option) => {
        var province_data = option.province;
        province_data.forEach((option) => {
          if (province_value == option.province_id) {
            ward.append($("<option>", {
              value: "",
              text: "Select ward",
              selected: true,
              disabled: true
            }));
            var ward_data = option.ward;
            ward_data.forEach((option) => {
              var isSelected = (parseInt(option.ward_id) == ward_value);
              ward.append($("<option>", {
                value: option.ward_id,
                text: option.name,
                selected: isSelected
              }));
            });
          }
        });
      });
    }

    province.change(function () {
      ward.empty();
      data.forEach((option) => {
        var province_data = option.province;
        province_data.forEach((option) => {
          if ($(this).val() == option.province_id) {
            ward.append($("<option>", {
              value: "",
              text: "Select ward",
              selected: true,
              disabled: true
            }));
            var ward_data = option.ward;
            ward_data.forEach((option) => {
              ward.append($("<option>", {
                value: option.ward_id,
                text: option.name
              }));
            });
          }
        });
      });
    });

    $('#editUserForm').submit(function (event) {
      event.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
        url: '/edit-profile',
        method: 'POST',
        data: formData,
        dataType: 'json',
        success: function (response) {
          // handle success
          location.reload();
        },
        error: function (xhr, status, error) {
          var response = JSON.parse(xhr.responseText);
          var errors = response.errors;
          for (var key in errors) {
            $('#' + key).addClass(' is-invalid');
            $('#error-' + key).show();
            $('#error-' + key).text(errors[key][0])
          }
        }
      });
    });

    var typeSuccess = $('#success-alert:hidden'),
      typeError = $('#error-alert:hidden'),
      isRtl = $('html').attr('data-textdirection') === 'rtl';

    if (typeSuccess.length) {
      toastr['success'](typeSuccess.text(), 'Success!', {
        showMethod: 'slideDown',
        hideMethod: 'slideUp',
        progressBar: true,
        closeButton: true,
        tapToDismiss: false,
        rtl: isRtl
      });
    }
    if (typeError.length) {
      toastr['error'](typeError.text(), 'Error!', {
        showMethod: 'slideDown',
        hideMethod: 'slideUp',
        progressBar: true,
        closeButton: true,
        tapToDismiss: false,
        rtl: isRtl
      });
    }

  });
});

/*=========================================================================================
  File Name: auth-login.js
  Description: Auth login js file.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: PIXINVENT
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(function () {
  'use strict';

  var pageLoginForm = $('.auth-login-form'),
    isRtl = $('html').attr('data-textdirection') === 'rtl',
    typeSuccess = $('#success-alert:hidden'),
    typeError = $('#error-alert:hidden');
  // jQuery Validation
  // --------------------------------------------------------------------
  if (pageLoginForm.length) {
    pageLoginForm.validate({
      rules: {
        'login_username': {
          required: true
        },
        'login_password': {
          required: true
        }
      }
    });
  }

  if (typeSuccess.length) {
    toastr['success'](typeSuccess.text(), 'Success!', {
      showMethod: 'slideDown',
      hideMethod: 'slideUp',
      progressBar: true,
      closeButton: true,
      tapToDismiss: false,
      rtl: isRtl
    });
  }
  if (typeError.length) {
    toastr['error'](typeError.text(), 'Error!', {
      showMethod: 'slideDown',
      hideMethod: 'slideUp',
      progressBar: true,
      closeButton: true,
      tapToDismiss: false,
      rtl: isRtl
    });
  }

});
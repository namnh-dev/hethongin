/*=========================================================================================
    File Name: calculate-print.js
    Description: Knowledge Base Page Script
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(function() {
    var isRtl = $('html').attr('data-textdirection') === 'rtl';
    'use strict';
    $('#loai-giay-option').each(function() {
        var selectedO = $('#loai-giay-option').find(":selected").val();
        if (selectedO == "optionO") {
            $('#dinh-luong-giay').hide();
            $('#E').removeAttr('hidden');
            $('#E-price').removeAttr('hidden');
        } else {
            if ($('#dinh-luong-giay').hide()) {
                $('#dinh-luong-giay').show();
                $('#E').attr("hidden", true);
                $('#E-price').attr("hidden", true);
            }
        }
    });

    $('#kho-may-option').each(function() {
        var selectedXY = $('#kho-may-option').find(":selected").val();
        if (selectedXY == "optionXY") {
            if (!$('#additionDiv1').attr('hidden') && !$('#additionDiv2').attr('hidden')) {
                $('#additionDiv1').attr("hidden", true);
                $('#additionDiv2').attr("hidden", true);
            }
            $('#X').removeAttr('hidden');
            $('#Y').removeAttr('hidden');
        } else {
            $('#additionDiv1').removeAttr('hidden');
            $('#additionDiv2').removeAttr('hidden');
            $('#X').attr("hidden", true);
            $('#Y').attr("hidden", true);
        }
    });

    $('#kho-giay-option').each(function() {
        var selectedU = $('#kho-giay-option').find(":selected").val();
        if (selectedU == "optionU") {
            if ($('#X').attr('hidden') && $('#Y').attr('hidden')) {
                $('#additionDiv1').removeAttr('hidden');
                $('#additionDiv2').removeAttr('hidden');
            }
            $('#I-1').removeAttr('hidden');
            $('#I-2').removeAttr('hidden');
        } else {
            $('#I-1').attr("hidden", true);
            $('#I-2').attr("hidden", true);
            $('#additionDiv1').attr("hidden", true);
            $('#additionDiv2').attr("hidden", true);
        }
    });

    $('#loai-giay-option').on('change', function() {
        var selectedO = $('#loai-giay-option').find(":selected").val();
        if (selectedO == "optionO") {
            $('#dinh-luong-giay').hide();
            $('#E').removeAttr('hidden');
            $('#E-price').removeAttr('hidden');
        } else {
            if ($('#dinh-luong-giay').hide()) {
                $('#dinh-luong-giay').show();
                $('#E').attr("hidden", true);
                $('#E-price').attr("hidden", true);
            }
        }
    });

    $('#kho-may-option').on('change', function() {
        var selectedXY = $('#kho-may-option').find(":selected").val();
        if (selectedXY == "optionXY") {
            if (!$('#additionDiv1').attr('hidden') && !$('#additionDiv2').attr('hidden')) {
                $('#additionDiv1').attr("hidden", true);
                $('#additionDiv2').attr("hidden", true);
            }
            $('#X').removeAttr('hidden');
            $('#Y').removeAttr('hidden');
        } else {
            $('#additionDiv1').removeAttr('hidden');
            $('#additionDiv2').removeAttr('hidden');
            $('#X').attr("hidden", true);
            $('#Y').attr("hidden", true);
        }
    });

    $('#kho-giay-option').on('change', function() {
        var selectedU = $('#kho-giay-option').find(":selected").val();
        if (selectedU == "optionU") {
            if ($('#X').attr('hidden') && $('#Y').attr('hidden')) {
                $('#additionDiv1').removeAttr('hidden');
                $('#additionDiv2').removeAttr('hidden');
            }
            $('#I-1').removeAttr('hidden');
            $('#I-2').removeAttr('hidden');
        } else {
            $('#I-1').attr("hidden", true);
            $('#I-2').attr("hidden", true);
            $('#additionDiv1').attr("hidden", true);
            $('#additionDiv2').attr("hidden", true);
        }
    });

    // On load Toast
});
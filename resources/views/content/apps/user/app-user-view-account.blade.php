@extends('layouts/contentLayoutMaster')

@section('title', 'User View - Account')

@section('vendor-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
    <section class="app-user-view-account">
        <div class="content-wrapper">
            <div class="content-body">
                <div class="row">
                    <!-- User Sidebar -->
                    <div class="col-xl-4 col-lg-5 col-md-5 order-0 order-md-0">
                        <!-- User Card -->
                        <div class="card">
                            <div class="card-body">
                                <div class="user-avatar-section">
                                    <div class="d-flex align-items-center flex-column">
                                        <img class="img-fluid rounded mt-3 mb-2"
                                            src="{{ asset('images/portrait/small/avatar-s-2.jpg') }}" height="110"
                                            width="110" alt="User avatar" />
                                        <div class="user-info text-center">
                                            <h4>{{ Auth::user()->fullname }}</h4>
                                            <span class="badge bg-light-secondary">Quản trị viên</span>
                                        </div>
                                    </div>
                                </div>
                                <div id="hidden-content" class="d-none d-sm-block">
                                    <div class="d-flex justify-content-start my-2 pt-60">
                                        <div class="d-flex align-items-start me-2">
                                            <span class="badge bg-light-primary p-75 rounded">
                                                <i data-feather="dollar-sign" class="font-medium-2"></i>
                                            </span>
                                            <div class="ms-75">
                                                <h4 class="mb-0">20.000 VNĐ</h4>
                                                <small>Số dư tài khoản</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-start my-2 pt-60">
                                        <div class="d-flex align-items-start">
                                            <span class="badge bg-light-primary p-75 rounded">
                                                <i data-feather="check-square" class="font-medium-2"></i>
                                            </span>
                                            <div class="ms-75">
                                                <h4 class="mb-0">1.23k</h4>
                                                <small>Hoàn thành nhiệm vụ</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-start my-2 pt-60">
                                        <div class="d-flex align-items-start">
                                            <span class="badge bg-light-primary p-75 rounded">
                                                <i data-feather="briefcase" class="font-medium-2"></i>
                                            </span>
                                            <div class="ms-75">
                                                <h4 class="mb-0">568</h4>
                                                <small>Dự án đã hoàn thành</small>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <h6 class="pt-50 mb-1" style="color: gray">THÔNG TIN CHI TIẾT</h6>
                                    <div class="info-container">
                                        <ul class="list-unstyled">
                                            <li class="mb-75">
                                                <span class="fw-bolder me-25">Tên:</span>
                                                <span>{{ Auth::user()->fullname }}</span>
                                            </li>
                                            <li class="mb-75">
                                                <span class="fw-bolder me-25">Email:</span>
                                                <span>{{ Auth::user()->email }}</span>
                                            </li>
                                            <li class="mb-75">
                                                <span class="fw-bolder me-25">Trạng thái:</span>
                                                @if (Auth::user()->status == 1)
                                                    <span class="badge bg-light-success">Tích cực</span>
                                                @else
                                                    <span class="badge bg-light-danger">Không tích cực</span>
                                                @endif
                                            </li>
                                            <li class="mb-75">
                                                <span class="fw-bolder me-25">Ngôn ngữ:</span>
                                                <span>English</span>
                                            </li>
                                            <li class="mb-75">
                                                <span class="fw-bolder me-25">Địa chỉ:</span>
                                                <span>{{ Auth::user()->address ? Auth::user()->address : 'NULL' }}</span>
                                            </li>
                                        </ul>
                                        <div class="d-flex justify-content-center pt-2">
                                            <a href="javascript:;" class="btn btn-primary me-1" data-bs-target="#editUser"
                                                data-bs-toggle="modal">
                                                Chỉnh sửa
                                            </a>
                                            <a href="javascript:;" class="btn btn-outline-danger suspend-user">Đình chỉ</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <a id="toggle-button" class="d-block d-sm-none mt-1">Xem thêm</a>
                                </div>
                            </div>
                        </div>
                        <!-- /User Card -->
                        <!-- Plan Card -->
                        <div class="card border-primary">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-start">
                                    <span class="badge bg-light-primary">Trải nghiệm</span>
                                    <div class="d-flex justify-content-center">
                                        <span class="fw-bolder display-5 mb-0 text-primary">20.000đ</span>
                                        <sub class="pricing-duration font-small-4 ms-25 mt-auto mb-2">/năm</sub>
                                    </div>
                                </div>
                                <ul class="ps-1 mb-2">
                                    <li class="mb-50">10 người dùng</li>
                                    <li class="mb-50">Bộ nhớ lên tới 10GB</li>
                                    <li>Hỗ trợ cơ bản</li>
                                </ul>
                                <div class="d-flex justify-content-between align-items-center fw-bolder mb-50">
                                    <span>Lượt sử dụng</span>
                                    <span>2 lượt/ ngày</span>
                                </div>
                                <div class="progress mb-50" style="height: 8px">
                                    <div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="50"
                                        aria-valuemax="100" aria-valuemin="0"></div>
                                </div>
                                <span>Còn lại 1 lượt sử dụng</span>
                                <div class="d-flex justify-content-center w-100 mt-2">
                                    <button class="btn btn-primary me-1" data-bs-target="#upgradePlanModal"
                                        data-bs-toggle="modal">
                                        Nâng cấp
                                    </button>
                                    <button disabled class="btn btn-secondary">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- /Plan Card -->
                    </div>
                    <!--/ User Sidebar -->

                    <!-- User Content -->
                    <div class="col-xl-8 col-lg-7 col-md-7 order-1 order-md-1">
                        <!-- User Pills -->
                        <ul class="nav nav-pills mb-2 d-flex">
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ asset('quan-ly/tai-khoan') }}">
                                    <i data-feather="user" class="font-medium-3 me-50"></i>
                                    <span class="fw-bold">Tổng quan</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ asset('quan-ly/bao-mat') }}">
                                    <i data-feather="lock" class="font-medium-3 me-50"></i>
                                    <span class="fw-bold">Bảo mật & Định danh</span>
                                </a>
                            </li>
                            <li class="nav-item d-none d-sm-block">
                                <a class="nav-link" href="{{ asset('app/user/view/billing') }}">
                                    <i data-feather="bookmark" class="font-medium-3 me-50"></i>
                                    <span class="fw-bold">Thanh toán & Kế hoạch</span>
                                </a>
                            </li>
                            <li class="nav-item d-none d-sm-block">
                                <a class="nav-link" href="{{ asset('app/user/view/notifications') }}">
                                    <i data-feather="bell" class="font-medium-3 me-50"></i><span
                                        class="fw-bold">Notifications</span>
                                </a>
                            </li>
                            <li class="nav-item d-none d-sm-block">
                                <a class="nav-link" href="{{ asset('app/user/view/connections') }}">
                                    <i data-feather="link" class="font-medium-3 me-50"></i><span
                                        class="fw-bold">Connections</span>
                                </a>
                            </li>
                        </ul>
                        <!--/ User Pills -->

                        <!-- Project table -->
                        <div class="card">
                            <h4 class="card-header">Tài chính</h4>
                            <div class="card-body">
                                <div class="d-flex justify-content-around align-items-start">
                                    <div class="d-flex justify-content-start">
                                        <span class="fw-bolder display-5 mb-0 text-primary">20.000 </span>
                                        <sub class="h5 pricing-currency text-primary mt-1 mb-0">VNĐ</sub>
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <span class="fw-bolder display-5 mb-0 text-primary">20.000 </span>
                                        <sub class="h5 pricing-currency text-primary mt-1 mb-0">VNĐ</sub>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-around align-items-start">
                                    <div class="d-flex justify-content-start">
                                        <h6>Tài khoản chính</h6>
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <h6>Tài khoản phụ</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <form id="moneyForm">
                                        @csrf
                                        <div class="d-flex mb-1 mb-sm-0">
                                            <div class="flex-grow-1 form-floating">
                                                <input type="text" class="form-control" id="money_input"
                                                    placeholder="Nhập số tiền" name="moneyInput">
                                                <label for="money_input">Nhập số tiền</label>
                                            </div>
                                            <button type="submit" id="submitMoneyInForm" class="ms-2 btn btn-primary">Nạp tiền</button>
                                            <button class="btn btn-outline-primary" id="btnLoading" type="button" disabled style="display: none">
                                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                <span class="ms-25 align-middle">Loading...</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                <div class="justify-content-start mt-2">
                                    <div class="col-md-3">
                                        <h6 class="flex-grow-1">
                                            Hoặc chọn nhanh
                                        </h6>
                                    </div>
                                    <div class="col-md-9 d-flex justify-content-around">
                                        <button type="button" class="ms-2 btn btn-outline-secondary 120000">
                                            120.000đ
                                        </button>
                                        <button type="button" class="ms-2 btn btn-outline-secondary 300000">
                                            300.000đ
                                        </button>
                                        <button type="button" class="ms-2 btn btn-outline-secondary 500000">
                                            500.000đ
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /Project table -->

                        <!-- Activity Timeline -->
                        <div class="card">
                            <h4 class="card-header">Lịch sử thanh toán</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>DỊCH VỤ</th>
                                            <th>THỜI GIAN</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                Tên dịch vụ
                                            </td>
                                            <td>10, July 2021 20:07</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Tên dịch vụ
                                            </td>
                                            <td>10, July 2021 20:07</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Tên dịch vụ
                                            </td>
                                            <td>10, July 2021 20:07</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Tên dịch vụ
                                            </td>
                                            <td>10, July 2021 20:07</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Tên dịch vụ
                                            </td>
                                            <td>10, July 2021 20:07</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Tên dịch vụ
                                            </td>
                                            <td>10, July 2021 20:07</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /Activity Timeline -->

                        <!-- Invoice table -->
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Lịch sử giao dịch</h4>
                            </div>
                            <div class="table-responsive">
                                <table class="table text-nowrap text-center">
                                    <thead>
                                        <tr>
                                            <th>GIAO DỊCH</th>
                                            <th>TIỀN</th>
                                            <th>TRẠNG THÁI</th>
                                            <th>THỜI GIAN</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                Nạp tiền vào tài khoản chính
                                            </td>
                                            <td>
                                                + 300.000 đ
                                            </td>
                                            <td>
                                                <span class="badge rounded-pill badge-light-success me-1">Hoàn thành</span>
                                            </td>
                                            <td>10, July 2021 20:07</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Nạp tiền vào tài khoản chính
                                            </td>
                                            <td>
                                                + 300.000 đ
                                            </td>
                                            <td>
                                                <span class="badge rounded-pill badge-light-success me-1">Hoàn thành</span>
                                            </td>
                                            <td>10, July 2021 20:07</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Nạp tiền vào tài khoản chính
                                            </td>
                                            <td>
                                                + 300.000 đ
                                            </td>
                                            <td>
                                                <span class="badge rounded-pill badge-light-success me-1">Hoàn thành</span>
                                            </td>
                                            <td>10, July 2021 20:07</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Nạp tiền vào tài khoản chính
                                            </td>
                                            <td>
                                                + 300.000 đ
                                            </td>
                                            <td>
                                                <span class="badge rounded-pill badge-light-success me-1">Hoàn thành</span>
                                            </td>
                                            <td>10, July 2021 20:07</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Nạp tiền vào tài khoản chính
                                            </td>
                                            <td>
                                                + 300.000 đ
                                            </td>
                                            <td>
                                                <span class="badge rounded-pill badge-light-success me-1">Hoàn thành</span>
                                            </td>
                                            <td>10, July 2021 20:07</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /Invoice table -->
                    </div>
                    <!--/ User Content -->
                </div>
            </div>
        </div>
    </section>

    @include('content/_partials/_modals/modal-edit-user')
    @include('content/_partials/_modals/modal-upgrade-plan')
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    {{-- data table --}}
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/pages/modal-edit-user.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/app-user-view-account.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/app-user-view.js')) }}"></script>
@endsection

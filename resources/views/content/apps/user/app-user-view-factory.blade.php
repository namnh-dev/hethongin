@extends('layouts/contentLayoutMaster')

@section('title', 'User View - Factory')

@section('vendor-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap5.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
@endsection

@section('content')
    <section class="app-user-view-factory">
        <div class="content-wrapper">
            <div class="content-body">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 order-0 order-md-0">
                        <!-- User Card -->
                        <div class="card">
                            <div class="card-body">
                                <div class="user-avatar-section">
                                    <div class="d-flex">
                                        <div class="col-md-1 align-items-center">
                                            <img class="img-fluid rounded mt-3 mb-2"
                                                src="{{ asset('images/portrait/small/avatar-s-2.jpg') }}" height="110"
                                                width="110" alt="User avatar" />
                                        </div>
                                        <div class="col-md-8 p-3 align-items-center">
                                            <h2>Tên xưởng</h2>
                                            <div class="icon-wrapper mt-2">
                                                <i data-feather='map-pin'></i>
                                                <span class="me-3">67 Đ. Võ Chí Công, Nghĩa Đô, Cầu Giấy, Hà Nội, Việt
                                                    Nam</span>
                                                <span>20-02-2023</span>

                                            </div>
                                        </div>
                                        <div class="d-flex col-md-3 align-items-center justify-content-end">
                                            <button type="button" class="btn btn-primary waves-effect">
                                                <i data-feather='check'></i>
                                                <span>Đã xác minh</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /User Card -->
                    </div>

                    <!-- User Content -->
                    <div class="col-xl-12 col-lg-12 col-md-12 order-1 order-md-1">
                        <!-- User Pills -->
                        <ul class="nav nav-pills mb-2 d-flex">
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ asset('quan-ly/nha-in') }}">
                                    <i data-feather="user-check" class="font-medium-3 me-50"></i>
                                    <span class="fw-bold">Profile</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <i data-feather="users" class="font-medium-3 me-50"></i>
                                    <span class="fw-bold">Teams</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <i data-feather="grid" class="font-medium-3 me-50"></i>
                                    <span class="fw-bold">Projects</span>
                                </a>
                            </li>
                            <li class="nav-item d-none d-sm-block">
                                <a class="nav-link" href="#">
                                    <i data-feather="link" class="font-medium-3 me-50"></i><span
                                        class="fw-bold">Connections</span>
                                </a>
                            </li>
                        </ul>
                        <!--/ User Pills -->

                        <!-- User Sidebar -->
                        <div class="row">
                            <div class="col-xl-4 col-lg-4 col-md-4 order-0 order-md-0">
                                <!-- User Card -->
                                <div class="card">
                                    <div class="card-body">
                                        <h6 class="pt-50 mb-1" style="color: gray">THÔNG TIN XƯỞNG</h6>
                                        <div class="info-container">
                                            <ul class="list-unstyled">
                                                <li class="mb-75">
                                                    <i data-feather='user'></i>
                                                    <span class="fw-bolder me-25">Tên xưởng:</span>
                                                    <span>John Doe</span>
                                                </li>
                                                <li class="mb-75">
                                                    <i data-feather='shield'></i>
                                                    <span class="fw-bolder me-25">Trạng thái:</span>
                                                    <span class="badge badge-light-success">
                                                        <i data-feather='check'></i>
                                                        <span>Đã xác minh</span>
                                                    </span>
                                                </li>
                                                <li class="mb-75">
                                                    <i data-feather='flag'></i>
                                                    <span class="fw-bolder me-25">Địa chỉ:</span>
                                                    <span>67 Đ. Võ Chí Công, Nghĩa Đô, Cầu Giấy,
                                                        Hà Nội, Việt Nam</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <h6 class="pt-50 mb-1" style="color: gray">THÔNG TIN LIÊN HỆ</h6>
                                        <div class="info-container">
                                            <ul class="list-unstyled">
                                                <li class="mb-75">
                                                    <i data-feather='phone-call'></i>
                                                    <span class="fw-bolder me-25">Contact:</span>
                                                    <span>(+84) 34567890</span>
                                                </li>
                                                <li class="mb-75">
                                                    <i data-feather='twitter'></i>
                                                    <span class="fw-bolder me-25">Zalo:</span>
                                                    <span>
                                                        <span>(+84) 34567890</span>
                                                    </span>
                                                </li>
                                                <li class="mb-75">
                                                    <i data-feather='mail'></i>
                                                    <span class="fw-bolder me-25">Email:</span>
                                                    <span>john.doe@gmail.com</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- /User Card -->
                                <!-- Plan Card -->
                                <div class="card">
                                    <div class="card-body">
                                        <p class="pt-50 mb-1" style="color: gray">Công nghệ in và gia công</p>
                                        <ul class="ps-1 mb-2 list-style-icons">
                                            <li class="mb-50">
                                                <h6><i data-feather='check'></i>10 người dùng</h6>
                                            </li>
                                            <li class="mb-50">
                                                <h6><i data-feather='check'></i>Bộ nhớ lên tới 10GB</h6>
                                            </li>
                                            <li>
                                                <h6><i data-feather='check'></i>Hỗ trợ cơ bản</h6>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /Plan Card -->
                            </div>
                            <!--/ User Sidebar -->
                            <div class="d-flex flex-wrap col-xl-8 col-lg-8 col-md-8">
                                <div class="d-flex square mb-2 mx-2 col-md-3 col-4 align-items-center">
                                    <div class="mx-auto">
                                        <p style="color: black">IMG</p>
                                    </div>
                                </div>
                                <div class="d-flex square mb-2 mx-2 col-md-3 col-4 align-items-center">
                                    <div class="mx-auto">
                                        <p style="color: black">IMG</p>
                                    </div>
                                </div>
                                <div class="d-flex square mb-2 mx-2 col-md-3 align-items-center">
                                    <div class="mx-auto">
                                        <p style="color: black">IMG</p>
                                    </div>
                                </div>
                                <div class="d-flex square mb-2 mx-2 col-md-3 align-items-center">
                                    <div class="mx-auto">
                                        <p style="color: black">IMG</p>
                                    </div>
                                </div>
                                <div class="d-flex square mb-2 mx-2 col-md-3 align-items-center">
                                    <div class="mx-auto">
                                        <p style="color: black">IMG</p>
                                    </div>
                                </div>
                                <div class="d-flex square mb-2 mx-2 col-md-3 align-items-center">
                                    <div class="mx-auto">
                                        <p style="color: black">IMG</p>
                                    </div>
                                </div>
                                <div class="d-flex square mb-2 mx-2 col-md-3 align-items-center">
                                    <div class="mx-auto">
                                        <p style="color: black">IMG</p>
                                    </div>
                                </div>
                                <div class="d-flex square mb-2 mx-2 col-md-3 align-items-center">
                                    <div class="mx-auto">
                                        <p style="color: black">IMG</p>
                                    </div>
                                </div>
                                <div class="d-flex square mb-2 mx-2 col-md-3 align-items-center">
                                    <div class="mx-auto">
                                        <p style="color: black">IMG</p>
                                    </div>
                                </div>
                                <div class="d-flex square mb-2 mx-2 col-md-3 align-items-center">
                                    <div class="mx-auto">
                                        <p style="color: black">IMG</p>
                                    </div>
                                </div>
                                <div class="d-flex square mb-2 mx-2 col-md-3 align-items-center">
                                    <div class="mx-auto">
                                        <p style="color: black">IMG</p>
                                    </div>
                                </div>
                                <div class="d-flex square mb-2 mx-2 col-md-3 align-items-center">
                                    <div class="mx-auto">
                                        <p style="color: black">IMG</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ User Content -->
                </div>
            </div>
        </div>
    </section>

    @include('content/_partials/_modals/modal-edit-user')
    @include('content/_partials/_modals/modal-upgrade-plan')
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    {{-- data table --}}
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/pages/modal-edit-user.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/app-user-view-account.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/app-user-view.js')) }}"></script>
@endsection

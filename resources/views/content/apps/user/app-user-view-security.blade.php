@extends('layouts/contentLayoutMaster')

@section('title', 'User View - Security')

@section('vendor-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
    <section class="app-user-view-security">

        <div class="content-wrapper">
            <div class="content-body">
                <div class="row">
                    <!-- User Sidebar -->
                    <div class="col-xl-4 col-lg-5 col-md-5 order-0 order-md-0">
                        <!-- User Card -->
                        <div class="card">
                            <div class="card-body">
                                <div class="user-avatar-section">
                                    <div class="d-flex align-items-center flex-column">
                                        <img class="img-fluid rounded mt-3 mb-2"
                                            src="{{ asset('images/portrait/small/avatar-s-2.jpg') }}" height="110"
                                            width="110" alt="User avatar" />
                                        <div class="user-info text-center">
                                            <h4>{{ Auth::user()->fullname }}</h4>
                                            <span class="badge bg-light-secondary">Quản trị viên</span>
                                        </div>
                                    </div>
                                </div>
                                <div id="hidden-content" class="d-none d-sm-block">
                                    <div class="d-flex justify-content-start my-2 pt-60">
                                        <div class="d-flex align-items-start me-2">
                                            <span class="badge bg-light-primary p-75 rounded">
                                                <i data-feather="dollar-sign" class="font-medium-2"></i>
                                            </span>
                                            <div class="ms-75">
                                                <h4 class="mb-0">20.000 VNĐ</h4>
                                                <small>Số dư tài khoản</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-start my-2 pt-60">
                                        <div class="d-flex align-items-start">
                                            <span class="badge bg-light-primary p-75 rounded">
                                                <i data-feather="check-square" class="font-medium-2"></i>
                                            </span>
                                            <div class="ms-75">
                                                <h4 class="mb-0">1.23k</h4>
                                                <small>Hoàn thành nhiệm vụ</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-start my-2 pt-60">
                                        <div class="d-flex align-items-start">
                                            <span class="badge bg-light-primary p-75 rounded">
                                                <i data-feather="briefcase" class="font-medium-2"></i>
                                            </span>
                                            <div class="ms-75">
                                                <h4 class="mb-0">568</h4>
                                                <small>Dự án đã hoàn thành</small>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <h6 class="pt-50 mb-1" style="color: gray">THÔNG TIN CHI TIẾT</h6>
                                    <div class="info-container">
                                        <ul class="list-unstyled">
                                            <li class="mb-75">
                                                <span class="fw-bolder me-25">Tên:</span>
                                                <span>{{ Auth::user()->fullname }}</span>
                                            </li>
                                            <li class="mb-75">
                                                <span class="fw-bolder me-25">Email:</span>
                                                <span>{{ Auth::user()->email }}</span>
                                            </li>
                                            <li class="mb-75">
                                                <span class="fw-bolder me-25">Trạng thái:</span>
                                                @if (Auth::user()->status == 1)
                                                    <span class="badge bg-light-success">Tích cực</span>
                                                @else
                                                    <span class="badge bg-light-danger">Không tích cực</span>
                                                @endif
                                            </li>
                                            <li class="mb-75">
                                                <span class="fw-bolder me-25">Ngôn ngữ:</span>
                                                <span>English</span>
                                            </li>
                                            <li class="mb-75">
                                                <span class="fw-bolder me-25">Địa chỉ:</span>
                                                <span>{{ Auth::user()->address ? Auth::user()->address : 'NULL' }}</span>
                                            </li>
                                        </ul>
                                        <div class="d-flex justify-content-center pt-2">
                                            <a href="javascript:;" class="btn btn-primary me-1" data-bs-target="#editUser"
                                                data-bs-toggle="modal">
                                                Chỉnh sửa
                                            </a>
                                            <a href="javascript:;" class="btn btn-outline-danger suspend-user">Đình chỉ</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <a id="toggle-button" class="d-block d-sm-none mt-1">Xem thêm</a>
                                </div>
                            </div>
                        </div>
                        <!-- /User Card -->
                        <!-- Plan Card -->
                        <div class="card border-primary">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-start">
                                    <span class="badge bg-light-primary">Trải nghiệm</span>
                                    <div class="d-flex justify-content-center">
                                        <span class="fw-bolder display-5 mb-0 text-primary">20.000đ</span>
                                        <sub class="pricing-duration font-small-4 ms-25 mt-auto mb-2">/năm</sub>
                                    </div>
                                </div>
                                <ul class="ps-1 mb-2">
                                    <li class="mb-50">10 người dùng</li>
                                    <li class="mb-50">Bộ nhớ lên tới 10GB</li>
                                    <li>Hỗ trợ cơ bản</li>
                                </ul>
                                <div class="d-flex justify-content-between align-items-center fw-bolder mb-50">
                                    <span>Lượt sử dụng</span>
                                    <span>2 lượt/ ngày</span>
                                </div>
                                <div class="progress mb-50" style="height: 8px">
                                    <div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="50"
                                        aria-valuemax="100" aria-valuemin="0"></div>
                                </div>
                                <span>Còn lại 1 lượt sử dụng</span>
                                <div class="d-flex justify-content-center w-100 mt-2">
                                    <button class="btn btn-primary me-1" data-bs-target="#upgradePlanModal"
                                        data-bs-toggle="modal">
                                        Nâng cấp
                                    </button>
                                    <button disabled class="btn btn-secondary">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- /Plan Card -->
                    </div>
                    <!--/ User Sidebar -->

                    <!-- User Content -->
                    <div class="col-xl-8 col-lg-7 col-md-7 order-1 order-md-1">
                        <!-- User Pills -->
                        <ul class="nav nav-pills mb-2 d-flex">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ asset('quan-ly/tai-khoan') }}">
                                    <i data-feather="user" class="font-medium-3 me-50"></i>
                                    <span class="fw-bold">Tổng quan</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ asset('quan-ly/bao-mat') }}">
                                    <i data-feather="lock" class="font-medium-3 me-50"></i>
                                    <span class="fw-bold">Bảo mật & Định danh</span>
                                </a>
                            </li>
                            <li class="nav-item d-none d-sm-block">
                                <a class="nav-link" href="{{ asset('app/user/view/billing') }}">
                                    <i data-feather="bookmark" class="font-medium-3 me-50"></i>
                                    <span class="fw-bold">Thanh toán & Kế hoạch</span>
                                </a>
                            </li>
                            <li class="nav-item d-none d-sm-block">
                                <a class="nav-link" href="{{ asset('app/user/view/notifications') }}">
                                    <i data-feather="bell" class="font-medium-3 me-50"></i><span
                                        class="fw-bold">Notifications</span>
                                </a>
                            </li>
                            <li class="nav-item d-none d-sm-block">
                                <a class="nav-link" href="{{ asset('app/user/view/connections') }}">
                                    <i data-feather="link" class="font-medium-3 me-50"></i><span
                                        class="fw-bold">Connections</span>
                                </a>
                            </li>
                        </ul>
                        <!--/ User Pills -->

                        <!-- Change Password -->
                        <div class="card">
                            <h4 class="card-header">Change Password</h4>
                            <div class="card-body">
                                <form id="formChangePassword" method="POST" action="{{ URL::to('/change-password') }}">
                                    @csrf
                                    <div class="alert alert-warning mb-2" role="alert">
                                        <h6 class="alert-heading">Đảm bảo rằng các yêu cầu này được đáp ứng</h6>
                                        <div class="alert-body fw-normal">Dài tối thiểu 8 kí tự, chữ hoa & kí hiệu
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="mb-2 col-md-6 form-password-toggle">
                                            <label class="form-label" for="oldPassword">Mật khẩu mới</label>
                                            <div class="input-group input-group-merge form-password-toggle">
                                                <input class="form-control" type="password" id="oldPassword"
                                                    name="oldPassword" placeholder="Nhập mật khẩu cũ"
                                                    value="{{ old('oldPassword') }}" />
                                                <span class="input-group-text cursor-pointer">
                                                    <i data-feather="eye"></i>
                                                </span>
                                            </div>
                                            @error('oldPassword')
                                                <span style="color: red">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="mb-2 col-md-6 form-password-toggle">
                                            <label class="form-label" for="newPassword">Xác nhận mật khẩu</label>
                                            <div class="input-group input-group-merge">
                                                <input class="form-control" type="password" name="newPassword"
                                                    id="newPassword" placeholder="Nhập mật khẩu mới"
                                                    value="{{ old('newPassword') }}" />
                                                <span class="input-group-text cursor-pointer"><i
                                                        data-feather="eye"></i></span>
                                            </div>
                                            @error('newPassword')
                                                <span style="color: red">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div>
                                            <button type="submit" class="btn btn-primary me-2">Đổi mật khẩu</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!--/ Change Password -->

                        <!-- Change phone number -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title mb-50">Đổi số điện thoại</h4>
                                <span>Keep your account secure with authentication step.</span>
                                <h6 class="fw-bolder mt-2">Tin nhắn</h6>
                                <div class="d-flex justify-content-between border-bottom mb-1 pb-1">
                                    <span>+1(968) 945-8832</span>
                                    <div class="action-icons">
                                        <a href="javascript:void(0)" class="text-body me-50"
                                            data-bs-target="#twoFactorAuthModal" data-bs-toggle="modal">
                                            <i data-feather="edit" class="font-medium-3"></i>
                                        </a>
                                        <a href="javascript:void(0)" class="text-body"><i data-feather="trash"
                                                class="font-medium-3"></i></a>
                                    </div>
                                </div>
                                <p class="mb-0">
                                    Xác thực hai yếu tố thêm một lớp bảo mật bổ sung vào tài khoản của bạn bằng cách yêu cầu
                                    nhiều hơn là mật khẩu để đăng nhập.
                                    <a href="javascript:void(0);" class="text-body">Tìm hiểu thêm.</a>
                                </p>
                            </div>
                        </div>
                        <!--/ Change phone number -->

                        <!-- Verify infomation -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title mb-50">Định danh thông tin</h4>
                                <span>Keep your account secure with authentication step.</span>
                                <h6 class="fw-bolder mt-2">Số CMT/CCCD</h6>
                                <div class="d-flex justify-content-between border-bottom mb-1 pb-1">
                                    <input type="text" class="form-control" value="12345678">
                                </div>
                                <!-- remove thumbnail file upload starts -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="#" class="dropzone dropzone-area dpz-accept-files">
                                                    <div class="dz-message">Mặt trước</div>
                                                    <span class="d-flex mt-3 justify-content-center">Kéo thả ảnh được chọn
                                                        vào đây để tại lên</span>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="#" class="dropzone dropzone-area dpz-accept-files">
                                                    <div class="dz-message">Mặt sau</div>
                                                    <span class="d-flex mt-3 justify-content-center">Kéo thả ảnh được chọn
                                                        vào đây để tại lên</span>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- remove thumbnail file upload ends -->
                            </div>
                        </div>
                        <!--/ Verify infomation -->

                        <!-- recent device -->
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Lịch sử đăng nhập</h4>
                            </div>
                            <div class="table-responsive">
                                <table class="table text-nowrap text-center">
                                    <thead>
                                        <tr>
                                            <th class="text-start">Trình duyệt</th>
                                            <th>Thiết bị</th>
                                            <th>Vị trí</th>
                                            <th>Hoạt động gần đây</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-start">
                                                <img class="me-75" src="{{ asset('images/icons/windows.png') }}"
                                                    alt="avatar" width="20" height="20" />
                                                <span class="fw-bolder">Chrome on Windows</span>
                                            </td>
                                            <td>HP Specter 360</td>
                                            <td>Switzerland</td>
                                            <td>10, July 2021 20:07</td>
                                        </tr>
                                        <tr>
                                            <td class="text-start">
                                                <img class="me-75" src="{{ asset('images/icons/mobile.png') }}"
                                                    alt="avatar" width="20" height="20" />
                                                <span class="fw-bolder">Chrome on Android</span>
                                            </td>
                                            <td>Google Pixel 3a</td>
                                            <td>Ghana</td>
                                            <td>11, Jan 2021 10:16</td>
                                        </tr>
                                        <tr>
                                            <td class="text-start">
                                                <img class="me-75" src="{{ asset('images/icons/android.png') }}"
                                                    alt="avatar" width="20" height="20" />
                                                <span class="fw-bolder">Chrome on Iphone</span>
                                            </td>
                                            <td>iPhone 12x</td>
                                            <td>Australia</td>
                                            <td>13, July 2021 10:10</td>
                                        </tr>
                                        <tr>
                                            <td class="text-start">
                                                <img class="me-75" src="{{ asset('images/icons/apple.png') }}"
                                                    alt="avatar" width="20" height="20" />
                                                <span class="fw-bolder">Chrome on MACOS</span>
                                            </td>
                                            <td>OnePlus 9 Pro</td>
                                            <td>Dubai</td>
                                            <td>4, July 2021 15:15</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- / recent device -->
                    </div>
                    <!--/ User Content -->
                </div>
            </div>
        </div>
        <!-- Alert-->
        @if (Session::has('success'))
            <div id="success-alert" hidden>
                {{ Session::get('success') }}
            </div>
        @endif
        
        @if (Session::has('error'))
            <div id="error-alert" hidden>
                {{ Session::get('error') }}
            </div>
        @endif
        <!-- Alert-->
    </section>


    @include('content/_partials/_modals/modal-edit-user')
    @include('content/_partials/_modals/modal-upgrade-plan')
    @include('content/_partials/_modals/modal-two-factor-auth')
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/pages/modal-two-factor-auth.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/modal-edit-user.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/app-user-view-security.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/app-user-view.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
@endsection

@extends('layouts/contentLayoutMaster')

@section('title', 'Calculate print')

@section('vendor-style')
{{-- Vendor Css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection


@section('content')
<section class="calculate-print">

    <div class="row match-height">
        <div class="col-md-6 col-12">

            <div class="card">
                <div class="card-header">
                    <h2 class="card-title">Nhập số liệu tính toán</h2>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <form class="form" id="jquery-val-form" action="{{ URL::to('/calculate/printing') }}"
                            method="POST">
                            @csrf
                            @if ($errors->any())
                            <!-- Basic toast -->
                            <div class="toast-container">
                                <div class="toast basic-toast position-fixed top-0 end-0 m-2" role="alert"
                                    aria-live="assertive" aria-atomic="true">
                                    <div class="toast-header">
                                        <img src="{{ asset('images/logo/logo.png') }}" class="me-1" alt="Toast image"
                                            height="18" width="25" />
                                        <strong class="me-auto">OOPS!! Something went wrong</strong>
                                        <small class="text-muted">Just now</small>
                                        <button type="button" class="ms-1 btn-close" data-bs-dismiss="toast"
                                            aria-label="Close"></button>
                                    </div>
                                    @error('chieu_ngang')
                                    <div class="toast-body">{{ $message }}</div>
                                    @enderror
                                    @error('chieu_doc')
                                    <div class="toast-body">{{ $message }}</div>
                                    @enderror
                                    @error('X_input')
                                    <div class="toast-body">{{ $message }}</div>
                                    @enderror
                                    @error('Y_input')
                                    <div class="toast-body">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <!-- <div class="position-fixed top-0 end-0 p-2">
                          </div> -->
                            <!-- Basic toast ends -->
                            <div id="errorMsg" hidden></div>
                            @endif
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="mb-1">
                                            <label for="chieu-ngang">Chiều ngang (A)*</label>
                                            <input type="text" id="chieu-ngang" class="form-control" name="chieu_ngang"
                                                placeholder="Nhập chiều ngang" value="{{ old('chieu_ngang') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="mb-1">
                                            <label for="chieu-doc">Chiều dọc (B)*</label>
                                            <input type="text" id="chieu-doc" class="form-control" name="chieu_doc"
                                                placeholder="Nhập chiều dọc" value="{{ old('chieu_doc') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="mb-1">
                                            <label for="so-luong">Số lượng (Z)*</label>
                                            <input type="text" id="so-luong" class="form-control" name="so_luong"
                                                placeholder="Nhập số lượng" value="{{ old('so_luong') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="mb-1">
                                            <label for="so-mat-in">Số mặt in (W)*</label>
                                            <select class="form-select" name="so_mat_in" id="so-mat-in">
                                                <option disabled {{ old('so_mat_in')==null ? 'selected' : '' }}>Chọn
                                                    số mặt in</option>
                                                <option value="1" {{ old('so_mat_in')=='1' ? 'selected' : '' }}>
                                                    1
                                                </option>
                                                <option value="2" {{ old('so_mat_in')=='2' ? 'selected' : '' }}>
                                                    2
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label for="loai-giay-option">Loại giấy (O)*</label>
                                            <select class="form-select" id="loai-giay-option" name="loai_giay_option">
                                                <option disabled selected>Chọn loại giấy</option>
                                                <option value="offset" {{ old('loai_giay_option')=='offset' ? 'selected'
                                                    : '' }}>Offset
                                                </option>
                                                <option value="ivory" {{ old('loai_giay_option')=='ivory' ? 'selected'
                                                    : '' }}>Ivory
                                                </option>
                                                <option value="couche" {{ old('loai_giay_option')=='couche' ? 'selected'
                                                    : '' }}>Couche
                                                </option>
                                                <option value="couchepindo" {{ old('loai_giay_option')=='couchepindo'
                                                    ? 'selected' : '' }}>
                                                    Couche Pindo</option>
                                                <option value="optionO" {{ old('loai_giay_option')=='optionO'
                                                    ? 'selected' : '' }}>Tùy
                                                    chỉnh</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12" id="E" hidden>
                                        <div class="mb-1">
                                            <label for="dinh-luong-giay-input">Định lượng giấy (E)*</label>
                                            <input type="text" id="dinh-luong-giay-input" class="form-control"
                                                name="dinh_luong_giay_input" placeholder="Nhập số lượng"
                                                value="{{ old('dinh_luong_giay_input') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12" id="E-price" hidden>
                                        <div class="mb-1">
                                            <label for="gia-giay-input">Giá giấy (E**)*</label>
                                            <input type="text" id="gia-giay-input" class="form-control"
                                                name="gia_giay_input" placeholder="Nhập số lượng"
                                                value="{{ old('gia_giay_input') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12" id="dinh-luong-giay">
                                        <div class="mb-1">
                                            <label for="dinh-luong-giay-option">Định lượng giấy (E)*</label>
                                            <select class="form-select" id="dinh-luong-giay-option"
                                                name="dinh_luong_giay_option">
                                                <option disabled selected>Chọn định lượng giấy</option>
                                                <option value="80" {{ old('dinh_luong_giay_option')=='80' ? 'selected'
                                                    : '' }}>80
                                                </option>
                                                <option value="100" {{ old('dinh_luong_giay_option')=='100' ? 'selected'
                                                    : '' }}>100
                                                </option>
                                                <option value="120" {{ old('dinh_luong_giay_option')=='120' ? 'selected'
                                                    : '' }}>120
                                                </option>
                                                <option value="140" {{ old('dinh_luong_giay_option')=='140' ? 'selected'
                                                    : '' }}>140
                                                </option>
                                                <option value="180" {{ old('dinh_luong_giay_option')=='180' ? 'selected'
                                                    : '' }}>180
                                                </option>
                                                <option value="200" {{ old('dinh_luong_giay_option')=='200' ? 'selected'
                                                    : '' }}>200
                                                </option>
                                                <option value="250" {{ old('dinh_luong_giay_option')=='250' ? 'selected'
                                                    : '' }}>250
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label for="kho-may-option">Khổ máy (X, Y)*</label>
                                            <select class="form-select" name="kho_may_option" id="kho-may-option">
                                                <option disabled selected>Chọn khổ máy</option>
                                                <option value="360x520" {{ old('kho_may_option')=='360x520' ? 'selected'
                                                    : '' }}>360x520
                                                </option>
                                                <option value="520x720" {{ old('kho_may_option')=='520x720' ? 'selected'
                                                    : '' }}>520x720
                                                </option>
                                                <option value="790x1090" {{ old('kho_may_option')=='790x1090'
                                                    ? 'selected' : '' }}>
                                                    790x1090
                                                </option>
                                                <option value="optionXY" {{ old('kho_may_option')=='optionXY'
                                                    ? 'selected' : '' }}>Tùy
                                                    chỉnh</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6 col-12"> -->
                                    <!-- <div class="mb-1">
                                                <label for="kho-giay-option">Khổ giấy (U)*</label>
                                                <select class="form-select" id="kho-giay-option" name="kho_giay_option">
                                                    <option disabled selected>Chọn khổ giấy</option>
                                                    <option value="650x860"
                                                        {{ old('kho_giay_option') == '650x860' ? 'selected' : '' }}>650x860
                                                    </option>
                                                    <option value="790x1090"
                                                        {{ old('kho_giay_option') == '790x1090' ? 'selected' : '' }}>
                                                        790x1090</option>
                                                    <option value="optionU">Tùy chỉnh</option>
                                                </select>
                                            </div> -->
                                    <!-- </div> -->
                                    <div class="col-md-3 col-12" id="X" hidden>
                                        <div class="mb-1">
                                            <label for="X-input">Khổ máy (X)</label>
                                            <input type="text" id="X-input" class="form-control" name="X_input"
                                                placeholder="Nhập khổ X" value="{{ old('X_input') }}">
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-12" id="Y" hidden>
                                        <div class="mb-1">
                                            <label for="Y-input">Khổ máy (Y)</label>
                                            <input type="text" id="Y-input" class="form-control" name="Y_input"
                                                placeholder="Nhập khổ Y" value="{{ old('Y_input') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12" hidden id="additionDiv1"></div>
                                    <div class="col-md-3 col-12" hidden id="additionDiv2"></div>
                                    <div class="col-md-3 col-12" id="I-1" hidden>
                                        <div class="mb-1">
                                            <label for="kho-giay-1-input">Khổ giấy (I)*</label>
                                            <input type="text" id="kho-giay-1-input" class="form-control"
                                                name="kho_giay_1_input" placeholder="Nhập khổ giấy"
                                                value="{{ old('kho_giay_1_input') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12" id="I-2" hidden>
                                        <div class="mb-1">
                                            <label for="kho-giay-2-input">Khổ giấy (I*)*</label>
                                            <input type="text" id="kho-giay-2-input" class="form-control"
                                                name="kho_giay_2_input" placeholder="Nhập khổ giấy"
                                                value="{{ old('kho_giay_2_input') }}">
                                        </div>
                                    </div>
                                    <div class="card-header">
                                        <h2 class="card-title">Gia công</h2>
                                    </div>
                                    <div class="mb-1 col-12">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" id="can-mang-checkbox"
                                                value="checked" checked>
                                            <label class="form-check-label" for="can-mang-checkbox">Cán
                                                màng</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label for="loai-can-mang">Loại cán màng</label>
                                            <select class="form-select" id="loai-can-mang">
                                                <option disabled selected>Tùy chọn</option>
                                                <option>Blade Runner</option>
                                                <option>Thor Ragnarok</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-6">
                                        <div class="mb-1">
                                            <label for="so-luong-can-mang"></label>
                                            <input type="text" id="so-luong-can-mang" class="form-control"
                                                name="so-luong-can-mang" placeholder="Nhập đơn giá...">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-6">
                                        <div class="mb-1">
                                            <label for="mat-in"></label>
                                            <select class="form-select" id="mat-in">
                                                <option disabled selected>Mặt in</option>
                                                <option>1</option>
                                                <option>2</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mb-1 col-12">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" id="ep-kim-checkbox"
                                                value="checked" checked>
                                            <label class="form-check-label" for="ep-kim-checkbox">Ép kim</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <div class="mb-1">
                                            <label for="chieu-dai">Chiều dài</label>
                                            <input type="text" id="chieu-dai" class="form-control" name="chieu-dai"
                                                placeholder="Nhập chiều dài">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <div class="mb-1">
                                            <label for="chieu-rong">Chiều rộng</label>
                                            <input type="text" id="chieu-rong" class="form-control" name="chieu-rong"
                                                placeholder="Nhập chiều rộng">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <div class="mb-1">
                                            <label for="khuon-ep-kim">Đơn giá khuôn ép kim (m&#178;)</label>
                                            <input type="text" id="khuon-ep-kim" class="form-control"
                                                name="khuon-ep-kim" placeholder="Nhập đơn giá">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <div class="mb-1">
                                            <label for="luot-ep-kim">Đơn giá lượt ép kim (m&#178;)</label>
                                            <input type="text" id="luot-ep-kim" class="form-control" name="luot-ep-kim"
                                                placeholder="Nhập đơn giá">
                                        </div>
                                    </div>
                                    <div class="mb-1 col-12">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" id="can-gap-checkbox"
                                                value="checked" checked>
                                            <label class="form-check-label" for="can-gap-checkbox">Cần gấp,
                                                demi</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label for="tien-luot-chay">Tiền lượt chạy</label>
                                            <input type="text" id="tien-luot-chay" class="form-control"
                                                name="tien-luot-chay" placeholder="Nhập tiền lượt chạy">
                                        </div>
                                    </div>
                                    <div class="mb-1 col-12">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" id="be-khuon-checkbox"
                                                value="checked" checked>
                                            <label class="form-check-label" for="be-khuon-checkbox">Bế
                                                khuôn</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <div class="mb-1">
                                            <label for="gia-khuon-be">Đơn giá khuôn bế (m&#178;)</label>
                                            <input type="text" id="gia-khuon-be" class="form-control"
                                                name="gia-khuon-be" placeholder="Nhập đơn giá">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <div class="mb-1">
                                            <label for="don-gia-luot">Đơn giá lượt (m&#178;)</label>
                                            <input type="text" id="don-gia-luot" class="form-control"
                                                name="don-gia-luot" placeholder="Nhập đơn giá">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-6">
                                        <div class="mb-1">
                                            <button type="submit" class="btn btn-primary btn-lg mr-1 mb-1">In
                                                nhanh</button>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-6">
                                        <div class="mb-1">
                                            <button type="button" class="btn btn-outline-primary btn-lg mr-1 mb-1">In
                                                offset</button>
                                        </div>
                                    </div>
                                    @if (Session::has('resultErr'))
                                    <div style="color: red" class="result">
                                        {!! Session::get('resultErr') !!}
                                    </div>
                                    @endif

                                    @if (Session::has('resultVer'))
                                    <div style="color: green" class="resultVer checkEle">
                                        {!! Session::get('resultVer') !!}
                                    </div>
                                    @endif

                                    @if (Session::has('resultHor'))
                                    <div style="color: green" class="resultHor checkEle">
                                        {!! Session::get('resultHor') !!}
                                    </div>
                                    @endif

                                    @if (Session::has('resultVerRev'))
                                    <div style="color: green" class="resultVerRev checkEle">
                                        {!! Session::get('resultVerRev') !!} after reverse A and B
                                    </div>
                                    @endif

                                    @if (Session::has('resultHorRev'))
                                    <div style="color: green" class="resultHorRev checkEle">
                                        {!! Session::get('resultHorRev') !!} after reverse A and B
                                    </div>
                                    @endif

                                    @if (Session::has('resultVer2'))
                                    <div style="color: green" class="resultVer2 checkEle">
                                        {!! Session::get('resultVer2') !!}
                                    </div>
                                    @endif

                                    @if (Session::has('resultHor2'))
                                    <div style="color: green" class="resultHor2 checkEle">
                                        {!! Session::get('resultHor2') !!}
                                    </div>
                                    @endif

                                    @if (Session::has('reverse'))
                                    <div style="color: green" id="reverse">after reverse A and B</div>
                                    @endif

                                    @if (Session::has('caseCoor'))
                                    <div id="caseCoor"></div>
                                    @endif

                                    @if (Session::has('newY'))
                                    <div id="newY" style="display: none">
                                        <p>{!! Session::get('newY') !!}</p>
                                    </div>
                                    @endif
                                    @if (Session::has('newX'))
                                    <div id="newX" style="display: none">
                                        <p>{!! Session::get('newX') !!}</p>
                                    </div>
                                    @endif

                                    @if (Session::has('mulPrintA'))
                                    <div id="mulPrintA"></div>
                                    @endif

                                    @if (Session::has('mulPrintB'))
                                    <div id="mulPrintB"></div>
                                    @endif
                                    <span id="coordinate" style="color: red"></span>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="col-md-6 col-12"> -->
        <div class="card">

            @php
            $allSolution = session::get('allSolution');
            if($allSolution)
            $num_pages = count($allSolution);
            else $num_pages = 0;
            @endphp
            <div class="card-header">
                <h2 class="card-title"><b>Kết quả tính toán</b></h2>
                <div class="card-header" style="display:flex; align-items: center;justify-content: center;">
                    <div class="card-title">
                        <button class="btn btn-primary btn-sm mr-1 mb-1" onclick="showPage(-1)"><
                                                </button>
                    </div>
                    <div class="card-title"
                        style="width:50px; padding:5px; display:flex; align-items: center;justify-content: center;">
                        <b class="mb-1" style="margin-left: 20px; margin-right: 20px;mb-1" id="current-page">1</b>
                    </div>
                    <div class="card-title">
                        <button type="submit" class="btn btn-primary btn-sm mr-1 mb-1" onclick="showPage(1)">></button>
                    </div>
                </div>
            </div>
            <!-- <div class="card-content">
                        <div class="row">
                            <table class="table">
                                <tr>
                                    <td></td>
                                    <td style="font-weight: bold;"><b>TH1. Xếp tối ưu</b></td>
                                    <td style="font-weight: bold;"><b>TH2. Xếp đều</b></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; color: red" ><b>Tổng chi phí </b> </td>
                                    <td style="font-weight: bold; color: red" id="tong-tien-span">{!! number_format(Session::get('totalCost1')) !!}</td>
                                    <td style="font-weight: bold; color: red" id="tong-tien-span">{!! number_format(Session::get('totalCost2')) !!}</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;" ><b>Chi phí máy in</b> </td>
                                    <td style="font-weight: bold;" id="tien-in-span">{!! number_format(Session::get('machineCost1')) !!}</td>
                                    <td style="font-weight: bold;" id="tien-in-span">{!! number_format(Session::get('machineCost2')) !!}</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;" ><b>Chi phí giấy in</b> </td>
                                    <td style="font-weight: bold;" id="tien-in-span">{!! number_format(Session::get('paperCost1')) !!}</td>
                                    <td style="font-weight: bold;" id="tien-in-span">{!! number_format(Session::get('paperCost2')) !!}</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;" ><b>Chi phí gia công</b> </td>
                                    <td style="font-weight: bold;" id="tien-in-span">0</td>
                                    <td style="font-weight: bold;" id="tien-in-span">0</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;" ><b>Khổ giấy đề xuất</b> </td>
                                    <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('khogiay1')) !!}</td>
                                    <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('khogiay2')) !!}</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;" ><b>Đơn giá giấy</b> </td>
                                    <td style="font-weight: bold;" id="tien-in-span">{!! number_format(Session::get('gia_giay1')) !!}</td>
                                    <td style="font-weight: bold;" id="tien-in-span">{!! number_format(Session::get('gia_giay2')) !!}</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;" ><b>Số lượng khổ giấy cần thiết</b> </td>
                                    <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('countPaper1')) !!} khổ</td>
                                    <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('countPaper2')) !!} khổ</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;" ><b>Kích thước cắt</b> </td>
                                    <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('sizeCrop1')) !!}</td>
                                    <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('sizeCrop2')) !!}</td>
                                </tr>

                                <tr>
                                <td style="font-weight: bold; color: red" id="tien-in-span">Thông tin chung</td>
                                </tr>
                                <tr>
                                    <td>Kích thước sản phẩm cần in</td>
                                    <td style="font-weight: bold" id="kich-thuoc-span">{!! Session::get('paperSize')!!}</td>
                                </tr>
                                <tr>
                                    <td>Số lượng cần in</td>
                                    <td style="font-weight: bold" id="so-luong-span">{!! number_format(Session::get('paperAmount')) !!}</td>
                                </tr>
                                <tr>
                                    <td>Số mặt in</td>
                                    <td style="font-weight: bold" id="mat-in-span">{!! number_format(Session::get('printType')) !!}</td>
                                </tr>
                                <tr>
                                    <td>Khổ máy in</td>
                                        <td style="font-weight: bold" id="kho-may-span">{!! Session::get('sizeMachine') !!}</td>
                                    </tr>
                                </tr>
                                <tr>
                                <td>
                                    (*) Hình ảnh minh họa cho các trường hợp<br></td>
                                    <!-- <td>(*) Hình ảnh minh họa. <br> Size giấy: {!! Session::get('paperSize')!!}</td> -->
            <!-- <td style="font-weight: bold" id="so-khoi-in-span"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="card-body" style="display: flex;align-items: center;justify-content: center;">
                            <hr>
                            @php
                                $base64image1_1 = session('base64image1_1');
                                $base64image1_2 = session('base64image1_2');
                                $base64image2_1 = session('base64image2_1');
                                $base64image2_2 = session('base64image2_2');
                                $khomay = session::get('sizeMachine');
                                $khogiay = session::get('khogiay');

                            @endphp

                            @if($base64image1_1)
                                <div><img src="data:image/png;base64,{{ $base64image1_1 }}" alt="Image" style="width: 80%; height: auto;border:1px solid black;">
                                <p>TH1. {{session('sizeCrop1')}}
                                    <br>  {{session('countPiece1')}} tờ/mặt
                                </p>
                            </div>
                            @endif
                            @if($base64image1_2)
                                <div><img src="data:image/png;base64,{{ $base64image1_2 }}" alt="Image" style="width: 80%; height: auto;border:1px solid black;">
                                <p>TH1. Cắt khổ {{session('khogiay1')}}<br>thành {{session('count1')}} phần.
                                </p>
                            </div>
                            @endif
                            @if($base64image2_1)
                                <div><img src="data:image/png;base64,{{ $base64image2_1 }}" alt="Image" style="width: 80%; height: auto;border:1px solid black;">
                                <p>TH2. {{session('sizeCrop2')}}
                                    <br> {{session('countPiece2')}} tờ/mặt
                                </p>
                            </div>
                            @endif
                            @if($base64image2_2)
                                <div><img src="data:image/png;base64,{{ $base64image2_2 }}" alt="Image" style="width: 80%; height: auto;border:1px solid black;">
                                <p>TH2. Cắt khổ {{session('khogiay2')}}<br>thành {{session('count2')}} phần.</p>
                            </div>
                            @endif
                        </div>
                    </div> -->

            @if($allSolution)
            <?php for ($i = 0; $i < count($allSolution); $i++): ?>
            @php
            $baseInfo = $allSolution[$i]['baseInfo'];
            // <!-- $solutionEasy =  $allSolution[$i]['solutionEasy']; -->
            // <!-- $solutionHard =  $allSolution[$i]['solutionHard']; -->
            $solution_1 = $allSolution[$i]['solution_1'];
            $solution_2 = $allSolution[$i]['solution_2'];
            $solution_3 = $allSolution[$i]['solution_3'];
            $solution_4 = $allSolution[$i]['solution_4'];
            @endphp

            <div class="card-content" id="result-page-{{$i}}" <?php echo ($i===0) ? 'style="display: block;"'
                : 'style="display: none;"' ; ?>>
                <div class="row">
                    <table class="table">
                        <tr>
                            <td style="font-weight: bold;; text-align: center; color: blue" colspan="1"><b>{!!
                                    $baseInfo['machineType']!!}</b></td>
                            <td style="font-weight: bold;; text-align: center; color: blue" colspan="2"><b>Khổ giấy
                                    650x860</b></td>
                            <td style="font-weight: bold;; text-align: center; color: blue" colspan="2"><b>Khổ giấy
                                    790x1090</b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="font-weight: bold; font-size: 16px; color: black; "><b>Xếp tối ưu</b></td>
                            <td style="font-weight: bold; font-size: 16px; color: black; "><b>Xếp đều</b></td>
                            <td style="font-weight: bold; font-size: 16px; color: black; "><b>Xếp tối ưu</b></td>
                            <td style="font-weight: bold; font-size: 16px; color: black; "><b>Xếp đều</b></td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; color: red"><b>Tổng chi phí </b> </td>
                            <td style="font-weight: bold; color: red" id="tong-tien-span">{!!
                                number_format($solution_1['totalCost']) !!}</td>
                            <td style="font-weight: bold; color: red" id="tong-tien-span">{!!
                                number_format($solution_2['totalCost']) !!}</td>
                            <td style="font-weight: bold; color: red" id="tong-tien-span">{!!
                                number_format($solution_3['totalCost']) !!}</td>
                            <td style="font-weight: bold; color: red" id="tong-tien-span">{!!
                                number_format($solution_4['totalCost']) !!}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;"><b>Chi phí máy in</b> </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format($solution_1['machineCost']) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format($solution_2['machineCost']) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format($solution_3['machineCost']) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format($solution_4['machineCost']) !!}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;"><b>Chi phí giấy in</b> </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! number_format($solution_1['paperCost'])
                                !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! number_format($solution_2['paperCost'])
                                !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! number_format($solution_3['paperCost'])
                                !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! number_format($solution_4['paperCost'])
                                !!}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;"><b>Chi phí gia công</b> </td>
                            <td style="font-weight: bold;" id="tien-in-span">0</td>
                            <td style="font-weight: bold;" id="tien-in-span">0</td>
                            <td style="font-weight: bold;" id="tien-in-span">0</td>
                            <td style="font-weight: bold;" id="tien-in-span">0</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;"><b>Số lượng khổ giấy cần thiết</b> </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! ($solution_1['countPaper']) !!} khổ
                            </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! ($solution_2['countPaper']) !!} khổ
                            </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! ($solution_3['countPaper']) !!} khổ
                            </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! ($solution_4['countPaper']) !!} khổ
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;"><b>Kích thước cắt</b> </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! ($solution_1['sizeCrop']) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! ($solution_2['sizeCrop']) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! ($solution_3['sizeCrop']) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! ($solution_4['sizeCrop']) !!}</td>
                        </tr>

                        <tr>
                            <td style="font-weight: bold; color: red" id="tien-in-span">Thông tin chung</td>
                        </tr>
                        <tr>
                            <td>Kích thước sản phẩm cần in</td>
                            <td style="font-weight: bold" id="kich-thuoc-span">{!! $baseInfo['paperSize'] !!}</td>
                        </tr>
                        <tr>
                            <td>Số lượng cần in</td>
                            <td style="font-weight: bold" id="so-luong-span">{!! number_format(
                                $baseInfo['paperAmount']) !!}</td>
                        </tr>
                        <tr>
                            <td>Số mặt in</td>
                            <td style="font-weight: bold" id="mat-in-span">{!! number_format( $baseInfo['printType'])
                                !!}</td>
                        </tr>
                        <tr>
                            <td>Khổ máy in</td>
                            <td style="font-weight: bold" id="kho-may-span">{!! $baseInfo['sizeMachine'] !!}</td>
                        </tr>
                        </tr>
                        <tr>
                            <td>
                                (*) Hình ảnh minh họa cho các trường hợp<br></td>
                            <!-- <td>(*) Hình ảnh minh họa. <br> Size giấy: {!! $baseInfo['paperSize'] !!}</td> -->
                            <td style="font-weight: bold" id="so-khoi-in-span"></td>
                        </tr>
                    </table>
                </div>
                <div class="card-body" style="display: flex;align-items: center;justify-content: center;">
                    <!-- <hr> -->
                    @php
                    $base64image1_1 = $solution_1['base64image_1'];
                    $base64image1_2 = $solution_1['base64image_2'];
                    $base64image2_1 = $solution_2['base64image_1'];
                    $base64image2_2 = $solution_2['base64image_2'];
                    $base64image3_1 = $solution_3['base64image_1'];
                    $base64image3_2 = $solution_3['base64image_2'];
                    $base64image4_1 = $solution_4['base64image_1'];
                    $base64image4_2 = $solution_4['base64image_2'];

                    @endphp
                    @if($base64image1_1)
                    <div><img src="data:image/png;base64,{{ $base64image1_1 }}" alt="Image"
                            style="width: 80%; height: auto;border:1px solid black;">
                        <p>TH1. {{$solution_1['sizeCrop']}}
                            <br> {{$solution_1['countPiece']}} tờ/mặt
                        </p>
                    </div>
                    @endif
                    @if($base64image1_2)
                    <div><img src="data:image/png;base64,{{ $base64image1_2 }}" alt="Image"
                            style="width: 80%; height: auto;border:1px solid black;">
                        <p>TH1. Cắt khổ {{$solution_1['khogiay']}}<br>thành {{$solution_1['count']}} phần.</p>
                    </div>
                    @endif
                    @if($base64image2_1)
                    <div><img src="data:image/png;base64,{{ $base64image2_1 }}" alt="Image"
                            style="width: 80%; height: auto;border:1px solid black;">
                        <p>TH2. {{$solution_2['sizeCrop']}}
                            <br> {{$solution_2['countPiece']}} tờ/mặt
                        </p>
                    </div>
                    @endif
                    @if($base64image2_2)
                    <div><img src="data:image/png;base64,{{ $base64image2_2 }}" alt="Image"
                            style="width: 80%; height: auto;border:1px solid black;">
                        <p>TH2. Cắt khổ {{$solution_2['khogiay']}}<br>thành {{$solution_2['count']}} phần.</p>
                    </div>
                    @endif
                    @if($base64image3_1)
                    <div><img src="data:image/png;base64,{{ $base64image3_1 }}" alt="Image"
                            style="width: 80%; height: auto;border:1px solid black;">
                        <p>TH2. {{$solution_3['sizeCrop']}}
                            <br> {{$solution_3['countPiece']}} tờ/mặt
                        </p>
                    </div>
                    @endif
                    @if($base64image3_1)
                    <div><img src="data:image/png;base64,{{ $base64image3_2 }}" alt="Image"
                            style="width: 80%; height: auto;border:1px solid black;">
                        <p>TH2. Cắt khổ {{$solution_3['khogiay']}}<br>thành {{$solution_3['count']}} phần.</p>
                    </div>
                    @endif
                    @if($base64image4_1)
                    <div><img src="data:image/png;base64,{{ $base64image4_1 }}" alt="Image"
                            style="width: 80%; height: auto;border:1px solid black;">
                        <p>TH2. {{$solution_4['sizeCrop']}}
                            <br> {{$solution_4['countPiece']}} tờ/mặt
                        </p>
                    </div>
                    @endif
                    @if($base64image4_2)
                    <div><img src="data:image/png;base64,{{ $base64image4_2 }}" alt="Image"
                            style="width: 80%; height: auto;border:1px solid black;">
                        <p>TH2. Cắt khổ {{$solution_4['khogiay']}}<br>thành {{$solution_4['count']}} phần.</p>
                    </div>
                    @endif
                </div>
            </div>
            <?php endfor; ?>
            @endif

            @if(!$allSolution)
            <div class="card-content">
                <div class="row">
                    <table class="table">
                        <tr>
                        <tr>
                            <td style="font-weight: bold;; text-align: center;" colspan="1"><b></b></td>
                            <td style="font-weight: bold;; text-align: center; color: blue" colspan="2"><b>Khổ giấy
                                    650x860</b></td>
                            <td style="font-weight: bold;; text-align: center; color: blue" colspan="2"><b>Khổ giấy
                                    790x1090</b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="font-weight: bold; font-size: 16px; color: black; text-align: center;"><b>Xếp tối
                                    ưu</b></td>
                            <td style="font-weight: bold; font-size: 16px; color: black; text-align: center;"><b>Xếp
                                    đều</b></td>
                            <td style="font-weight: bold; font-size: 16px; color: black; text-align: center;"><b>Xếp tối
                                    ưu</b></td>
                            <td style="font-weight: bold; font-size: 16px; color: black; text-align: center;"><b>Xếp
                                    đều</b></td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; color: red"><b>Tổng chi phí (VND)</b> </td>
                            <td style="font-weight: bold; color: red" id="tong-tien-span">{!!
                                number_format(Session::get('totalCost1')) !!}</td>
                            <td style="font-weight: bold; color: red" id="tong-tien-span">{!!
                                number_format(Session::get('totalCost2')) !!}</td>
                            <td style="font-weight: bold; color: red" id="tong-tien-span">{!!
                                number_format(Session::get('totalCost1')) !!}</td>
                            <td style="font-weight: bold; color: red" id="tong-tien-span">{!!
                                number_format(Session::get('totalCost1')) !!}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;"><b>Chi phí máy in</b> </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format(Session::get('machineCost1')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format(Session::get('machineCost2')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format(Session::get('machineCost1')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format(Session::get('machineCost2')) !!}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;"><b>Chi phí giấy in</b> </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format(Session::get('paperCost1')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format(Session::get('paperCost2')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format(Session::get('paperCost1')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format(Session::get('paperCost2')) !!}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;"><b>Chi phí gia công</b> </td>
                            <td style="font-weight: bold;" id="tien-in-span">0</td>
                            <td style="font-weight: bold;" id="tien-in-span">0</td>
                            <td style="font-weight: bold;" id="tien-in-span">0</td>
                            <td style="font-weight: bold;" id="tien-in-span">0</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;"><b>Khổ giấy đề xuất</b> </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('khogiay1')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('khogiay2')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('khogiay1')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('khogiay2')) !!}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;"><b>Đơn giá giấy</b> </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format(Session::get('gia_giay1')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format(Session::get('gia_giay2')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format(Session::get('gia_giay1')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!!
                                number_format(Session::get('gia_giay2')) !!}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;"><b>Số lượng khổ giấy cần thiết</b> </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('countPaper1')) !!} khổ
                            </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('countPaper2')) !!} khổ
                            </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('countPaper1')) !!} khổ
                            </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('countPaper2')) !!} khổ
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;"><b>Kích thước cắt</b> </td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('sizeCrop1')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('sizeCrop2')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('sizeCrop1')) !!}</td>
                            <td style="font-weight: bold;" id="tien-in-span">{!! (Session::get('sizeCrop2')) !!}</td>
                        </tr>

                        <tr>
                            <td style="font-weight: bold; color: red" id="tien-in-span">Thông tin chung</td>
                        </tr>
                        <tr>
                            <td>Kích thước sản phẩm cần in</td>
                            <td style="font-weight: bold" id="kich-thuoc-span">{!! Session::get('paperSize')!!}</td>
                        </tr>
                        <tr>
                            <td>Số lượng cần in</td>
                            <td style="font-weight: bold" id="so-luong-span">{!!
                                number_format(Session::get('paperAmount')) !!}</td>
                        </tr>
                        <tr>
                            <td>Số mặt in</td>
                            <td style="font-weight: bold" id="mat-in-span">{!! number_format(Session::get('printType'))
                                !!}</td>
                        </tr>
                        <tr>
                            <td>Khổ máy in</td>
                            <td style="font-weight: bold" id="kho-may-span">{!! Session::get('sizeMachine') !!}</td>
                        </tr>
                        </tr>
                        <tr>
                            <td>
                                (*) Hình ảnh minh họa cho các trường hợp<br></td>
                            <!-- <td>(*) Hình ảnh minh họa. <br> Size giấy: {!! Session::get('paperSize')!!}</td> -->
                            <td style="font-weight: bold" id="so-khoi-in-span"></td>
                        </tr>
                    </table>
                </div>
                <div class="card-body" style="display: flex;align-items: center;justify-content: center;">
                    <hr>
                    @php
                    $base64image1_1 = session('base64image1_1');
                    $base64image1_2 = session('base64image1_2');
                    $base64image2_1 = session('base64image2_1');
                    $base64image2_2 = session('base64image2_2');
                    $khomay = session::get('sizeMachine');
                    $khogiay = session::get('khogiay');

                    @endphp

                    @if($base64image1_1)
                    <div><img src="data:image/png;base64,{{ $base64image1_1 }}" alt="Image"
                            style="width: 80%; height: auto;border:1px solid black;">
                        <p>TH1. {{session('sizeCrop1')}}
                            <br> {{session('countPiece1')}} tờ/mặt
                        </p>
                    </div>
                    @endif
                    @if($base64image1_2)
                    <div><img src="data:image/png;base64,{{ $base64image1_2 }}" alt="Image"
                            style="width: 80%; height: auto;border:1px solid black;">
                        <p>TH1. Cắt khổ {{session('khogiay1')}}<br>thành {{session('count1')}} phần.
                        </p>
                    </div>
                    @endif
                    @if($base64image2_1)
                    <div><img src="data:image/png;base64,{{ $base64image2_1 }}" alt="Image"
                            style="width: 80%; height: auto;border:1px solid black;">
                        <p>TH2. {{session('sizeCrop2')}}
                            <br> {{session('countPiece2')}} tờ/mặt
                        </p>
                    </div>
                    @endif
                    @if($base64image2_2)
                    <div><img src="data:image/png;base64,{{ $base64image2_2 }}" alt="Image"
                            style="width: 80%; height: auto;border:1px solid black;">
                        <p>TH2. Cắt khổ {{session('khogiay2')}}<br>thành {{session('count2')}} phần.</p>
                    </div>
                    @endif
                </div>
            </div>

            @endif
        </div>
    </div>
    </div>
</section>
@endsection

@section('vendor-script')
<!-- vendor files -->
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection

@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/components/components-bs-toast.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
<script src="{{ asset('js/scripts/pages/calculate-print.js') }}"></script>
@endsection

<script>
    // Hàm để ẩn tất cả các trang và hiển thị trang được chọn
    function showPage(id) {
        // Ẩn tất cả các trang
        va r num_page = <?php echo $num_pages;  ?>;

      _page)
        {
            for (var i = 0; i < +)
            {
                document.getElementById("result-page-" + i).style.display = 'none';         }
    
            var currentPageElement = document.getElementById('cnt-page');
    
            // Hiển thị trang được chọn
            // document.getElementById(pageId + 'Page').style.display = 'block';
            var currentPageText = currentPageElement.innerText || currentPageElement.textContent;

            var next = parseInt(currentPageText) + id - 1; 
            if(ne e)
            {
                next = 0;
            }
             else if(next < 0) next = num_page - 1;

            currentPageElement.textContent = next + 1;

            document.getElementById("result-page-" + next).style.display = 'block';
     }
</script>
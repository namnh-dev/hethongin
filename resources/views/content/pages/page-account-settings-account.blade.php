@extends('layouts/contentLayoutMaster')

@section('title', 'Account')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
@endsection
@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <ul class="nav nav-pills mb-2">
                <!-- Account -->
                <li class="nav-item">
                    <a class="nav-link active" href="{{ asset('cau-hinh/nha-xuong') }}">
                        <i data-feather="user" class="font-medium-3 me-50"></i>
                        <span class="fw-bold">Thông tin xưởng</span>
                    </a>
                </li>
                <!-- security -->
                <li class="nav-item">
                    <a class="nav-link" href="{{ asset('cau-hinh/don-gia') }}">
                        <i data-feather="lock" class="font-medium-3 me-50"></i>
                        <span class="fw-bold">Cấu hình đơn giá</span>
                    </a>
                </li>
                <!-- billing and plans -->
                <li class="nav-item d-none d-sm-block">
                    <a class="nav-link" href="{{ asset('page/account-settings-billing') }}">
                        <i data-feather="file-text" class="font-medium-3 me-50"></i>
                        <span class="fw-bold">Billings &amp; Plans</span>
                    </a>
                </li>
                <!-- notification -->
                <li class="nav-item d-none d-sm-block">
                    <a class="nav-link" href="{{ asset('page/account-settings-notifications') }}">
                        <i data-feather="bell" class="font-medium-3 me-50"></i>
                        <span class="fw-bold">Notifications</span>
                    </a>
                </li>
                <!-- connection -->
                <li class="nav-item d-none d-sm-block">
                    <a class="nav-link" href="{{ asset('page/account-settings-connections') }}">
                        <i data-feather="link" class="font-medium-3 me-50"></i>
                        <span class="fw-bold">Connections</span>
                    </a>
                </li>
            </ul>

            <!-- profile -->
            <div class="card">
                <div class="card-header border-bottom">
                    <h4 class="card-title">Thông tin xưởng</h4>
                </div>
                <div class="card-body py-2 my-25">
                    <!-- header section -->
                    <div class="d-block d-lg-flex">
                        <a href="#" class="me-25 d-flex justify-content-center">
                            <img src="{{ asset('images/portrait/small/avatar-s-11.jpg') }}" id="account-upload-img"
                                class="uploadedAvatar rounded me-50" alt="profile image" height="100" width="100" />
                        </a>
                        <!-- upload and reset button -->
                        <div class="d-flex align-items-end mt-75 ms-1">
                            <div>
                                <label for="account-upload" class="btn btn-sm btn-primary p-1 mb-75 me-75">Tải ảnh mới
                                    lên</label>
                                <input type="file" id="account-upload" hidden accept="image/*" />
                                <button type="button" id="account-reset"
                                    class="btn btn-sm btn-outline-secondary px-3 py-1 mb-75 me-75">Cài lại</button>
                                <p class="mb-0 d-flex flex-column align-items-center text-center">Cho phép JPG, GIF hoặc PNG. Kích thước tối đa 800K</p>
                            </div>
                        </div>
                        <!--/ upload and reset button -->
                    </div>
                    <!--/ header section -->
                    <hr>

                    <!-- form -->
                    <form class="validate-form mt-2 pt-50">
                        <div class="row">
                            <div class="col-12 col-sm-6 mb-1">
                                <label class="form-label" for="ten-xuong">Tên xưởng</label>
                                <input type="text" class="form-control" id="ten-xuong" name="ten_xuong"
                                    placeholder="John" data-msg="Vui lòng nhập tên xưởng" />
                            </div>
                            <div class="col-12 col-sm-6 mb-1">
                                <label class="form-label" for="email">Email</label>
                                <input type="text" class="form-control" id="email" name="email"
                                    placeholder="john.doe@gmail.com" data-msg="Vui lòng nhập email" />
                            </div>
                            <div class="col-12 col-sm-6 mb-1">
                                <label class="form-label" for="phone">Số điện thoại</label>
                                <input type="text" class="form-control" id="phone" name="phone"
                                    placeholder="674 758 7365" />
                            </div>
                            <div class="col-12 col-sm-6 mb-1">
                                <label class="form-label" for="zalo">Zalo</label>
                                <input type="text" class="form-control" id="zalo" name="zalo"
                                    placeholder="674 758 7365" />
                            </div>
                            <h5 class="text-muted">Thông tin địa chỉ</h5>
                            <div class="col-12 col-sm-6 mb-1">
                                <label class="form-label" for="country">Tỉnh/ Thành phố</label>
                                <select id="country" class="select2 form-select">
                                    <option value="">Select Timezone</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Bangladesh">Bangladesh</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Brazil">Brazil</option>
                                    <option value="Canada">Canada</option>
                                    <option value="China">China</option>
                                    <option value="France">France</option>
                                    <option value="Germany">Germany</option>
                                    <option value="India">India</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Israel">Israel</option>
                                    <option value="Italy">Italy</option>
                                    <option value="Japan">Japan</option>
                                    <option value="Korea">Korea, Republic of</option>
                                    <option value="Mexico">Mexico</option>
                                    <option value="Philippines">Philippines</option>
                                    <option value="Russia">Russian Federation</option>
                                    <option value="South Africa">South Africa</option>
                                    <option value="Thailand">Thailand</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="Ukraine">Ukraine</option>
                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="United States">United States</option>
                                </select>
                            </div>
                            <div class="col-12 col-sm-6 mb-1">
                                <label for="language" class="form-label">Quận huyện</label>
                                <select id="language" class="select2 form-select">
                                    <option value="">Select Timezone</option>
                                    <option value="en">English</option>
                                    <option value="fr">French</option>
                                    <option value="de">German</option>
                                    <option value="us">USA</option>
                                </select>
                            </div>
                            <div class="col-12 col-sm-6 mb-1">
                                <label for="timeZones" class="form-label">Phường/ Xã</label>
                                <select id="timeZones" class="select2 form-select">
                                    <option value="">Select Timezone</option>
                                    <option value="-12">
                                        (GMT-12:00) International Date Line West
                                    </option>
                                    <option value="-11">
                                        (GMT-11:00) Midway Island, Samoa
                                    </option>
                                    <option value="-10">
                                        (GMT-10:00) Hawaii
                                    </option>
                                    <option value="-9">
                                        (GMT-09:00) Alaska
                                    </option>
                                    <option value="-8">
                                        (GMT-08:00) Pacific Time (US & Canada)
                                    </option>
                                    <option value="-8">
                                        (GMT-08:00) Tijuana, Baja California
                                    </option>
                                    <option value="-7">
                                        (GMT-07:00) Arizona
                                    </option>
                                    <option value="-7">
                                        (GMT-07:00) Chihuahua, La Paz, Mazatlan
                                    </option>
                                    <option value="-7">
                                        (GMT-07:00) Mountain Time (US & Canada)
                                    </option>
                                    <option value="-6">
                                        (GMT-06:00) Central America
                                    </option>
                                    <option value="-6">
                                        (GMT-06:00) Central Time (US & Canada)
                                    </option>
                                    <option value="-6">
                                        (GMT-06:00) Guadalajara, Mexico City, Monterrey
                                    </option>
                                    <option value="-6">
                                        (GMT-06:00) Saskatchewan
                                    </option>
                                    <option value="-5">
                                        (GMT-05:00) Bogota, Lima, Quito, Rio Branco
                                    </option>
                                    <option value="-5">
                                        (GMT-05:00) Eastern Time (US & Canada)
                                    </option>
                                    <option value="-5">
                                        (GMT-05:00) Indiana (East)
                                    </option>
                                    <option value="-4">
                                        (GMT-04:00) Atlantic Time (Canada)
                                    </option>
                                    <option value="-4">
                                        (GMT-04:00) Caracas, La Paz
                                    </option>
                                    <option value="-4">
                                        (GMT-04:00) Manaus
                                    </option>
                                    <option value="-4">
                                        (GMT-04:00) Santiago
                                    </option>
                                    <option value="-3.5">
                                        (GMT-03:30) Newfoundland
                                    </option>
                                </select>
                            </div>
                            <div class="col-12 col-sm-6 mb-1">
                                <label class="form-label" for="address">Địa chỉ chi tiết</label>
                                <input type="text" class="form-control" id="address" name="address"
                                    placeholder="Address" data-msg="Vui lòng nhập địa chỉ chi tiết" />
                            </div>
                            <div class="d-flex flex-wrap">
                                <button type="submit" class="btn btn-primary btn-block mt-1 me-1">Lưu thay đổi</button>
                                <button type="reset" class="btn btn-outline-secondary btn-block mt-1">Cài lại</button>
                            </div>
                        </div>
                    </form>
                    <!--/ form -->
                </div>
            </div>

            <!-- deactivate account  -->
            <div class="card">
                <div class="card-header border-bottom">
                    <h4 class="card-title">Xác minh nhà xưởng</h4>
                </div>
                <div class="card-body">
                    <h6 class="pt-50 mb-2">Tải ảnh nhà xưởng</h6>
                    <div class="d-flex flex-wrap">
                        <div class="d-flex rounded square mb-3 align-items-center"
                            style="width: 341px; height: 255px;">
                            <div class="mx-auto">
                                <p style="color: black">IMG</p>
                            </div>
                        </div>
                        <div class="ms-md-5 ms-sm-0 ms-0">
                            <form action="#" class="dropzone dropzone-area dpz-accept-files"
                                style="max-width: 340px; min-height: 250px;">
                                <div class="dz-message flex-column align-items-center text-center"
                                    style="font-size: 1.5rem; font-weight: bold; color: #4B465C">
                                    Thả tập tin ở đây hoặc nhấp để tải lên
                                </div>
                                <span class="d-flex mt-2 flex-column align-items-center text-center">
                                    Đây chỉ là dropzone demo. Các tệp
                                    được chọn không thực sự được tải lên.
                                </span>
                            </form>
                        </div>
                    </div>
                    <h6 class="pt-50 mb-2">Tải giấy tờ chứng minh nhà xưởng</h6>
                    <div class="d-flex flex-wrap">
                    <div class="d-flex rounded square mb-2 align-items-center"
                        style="width: 341px; height: 255px;">
                        <div class="mx-auto">
                            <p style="color: black">IMG</p>
                        </div>
                    </div>
                    <div class="ms-md-5 ms-sm-0 ms-0">
                        <form action="#" class="dropzone dropzone-area dpz-accept-files"
                            style="max-width: 340px; min-height: 250px;">
                            <div class="dz-message flex-column align-items-center text-center"
                                style="font-size: 1.5rem; font-weight: bold; color: #4B465C">
                                Thả tập tin ở đây hoặc nhấp để tải lên
                            </div>
                            <span class="d-flex mt-2 flex-column align-items-center text-center">
                                Đây chỉ là dropzone demo. Các tệp
                                được chọn không thực sự được tải lên.
                            </span>
                        </form>
                    </div>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary mt-1 me-1">Đăng ký xác nhận</button>
                    </div>
                </div>
            </div>
            <!--/ profile -->
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/pages/page-account-settings-account.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
@endsection

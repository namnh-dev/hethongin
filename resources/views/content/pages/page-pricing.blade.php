@extends('layouts/contentLayoutMaster')

@section('title', 'Pricing')

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/base/pages/page-pricing.css') }}">
@endsection

@section('content')
    <section id="pricing-plan">
        <div class="card">
            <div class="card-header d-flex justify-content-center">
                <!-- title text and switch button -->
                <div class="text-center">
                    <h1 class="mt-5">Pricing Plans</h1>
                    <p class="mb-2 pb-75">
                        All plans include 40+ advanced tools and features to boost your product.<br>
                        Choose the best plan to fit your needs.
                    </p>
                    <div class="d-flex align-items-center justify-content-center mb-5 pb-50">
                        <h6 class="me-1 mb-0">Monthly</h6>
                        <div class="form-check form-switch">
                            <input type="checkbox" class="form-check-input" id="priceSwitch" />
                            <label class="form-check-label" for="priceSwitch"></label>
                        </div>
                        <h6 class="ms-50 mb-0">Annual </h6>
                        <div class="pricing-badge text-end">
                            <span class="badge rounded-pill badge-light-primary">Save up to 10%</span>
                        </div>
                    </div>
                </div>
                <!--/ title text and switch button -->
            </div>
            <div class="card-body">

                <!-- pricing plan cards -->
                <div class="row pricing-card">
                    <div class="col-12 col-sm-offset-2 col-sm-10 col-md-12 col-lg-offset-2 col-lg-10 mx-auto">
                        <div class="row">
                            <!-- dùng thử plan -->
                            <div class="col-12 col-md-3">
                                <div class="card basic-pricing non-popular text-center">
                                    <div class="card-body">
                                        <img src="{{ asset('images/illustration/Pot1.svg') }}" class="mb-2 mt-5"
                                            alt="svg img" />
                                        <h3>Dùng thử</h3>
                                        <p class="card-text">A simple start for everyone</p>
                                        <div class="annual-plan">
                                            <div class="plan-price mt-2">
                                                <span class="pricing-basic-value fw-bolder text-primary">10.000đ</span>
                                                <sub class="pricing-duration text-body font-medium-1 fw-bold">/tháng</sub>
                                            </div>
                                            <small class="annual-pricing d-none text-muted"></small>
                                        </div>
                                        <ul class="list-group list-group-circle text-start">
                                            <li class="list-group-item">100 responses a month</li>
                                            <li class="list-group-item">Unlimited forms and surveys</li>
                                            <li class="list-group-item">Unlimited fields</li>
                                            <li class="list-group-item">Basic form creation tools</li>
                                            <li class="list-group-item">Up to 2 subdomains</li>
                                        </ul>
                                        <button class="btn w-100 btn-outline-success mt-2">Gói hiện tại của bạn</button>
                                    </div>
                                </div>
                            </div>
                            <!--/ dùng thử plan -->

                            <!-- trải nghiệm plan -->
                            <div class="col-12 col-md-3">
                                <div class="card standard-pricing popular text-center">
                                    <div class="card-body">
                                        <div class="pricing-badge text-end">
                                            <span class="badge rounded-pill badge-light-primary">Popular</span>
                                        </div>
                                        <img src="{{ asset('images/illustration/Pot2.svg') }}" class="mb-1"
                                            alt="svg img" />
                                        <h3>Trải nghiệm</h3>
                                        <p class="card-text">For small to medium businesses</p>
                                        <div class="annual-plan">
                                            <div class="plan-price mt-2">
                                                <span class="pricing-standard-value fw-bolder text-primary">20.000đ</span>
                                                <sub class="pricing-duration text-body font-medium-1 fw-bold">/năm</sub>
                                            </div>
                                            <small class="annual-pricing d-none text-muted"></small>
                                        </div>
                                        <ul class="list-group list-group-circle text-start">
                                            <li class="list-group-item">Unlimited responses</li>
                                            <li class="list-group-item">Unlimited forms and surveys</li>
                                            <li class="list-group-item">Instagram profile page</li>
                                            <li class="list-group-item">Google Docs integration</li>
                                            <li class="list-group-item">Custom “Thank you” page</li>
                                        </ul>
                                        <button class="btn w-100 btn-primary mt-2">Upgrade</button>
                                    </div>
                                </div>
                            </div>
                            <!--/ trải nghiệm plan -->

                            <!-- đồng plan -->
                            <div class="col-12 col-md-3">
                                <div class="card enterprise-pricing non-popular text-center">
                                    <div class="card-body">
                                        <img src="{{ asset('images/illustration/Pot3.svg') }}" class="mb-2"
                                            alt="svg img" />
                                        <h3>Đồng</h3>
                                        <p class="card-text">Solution for big organizations</p>
                                        <div class="annual-plan">
                                            <div class="plan-price mt-2">
                                                <span class="pricing-enterprise-value fw-bolder text-primary">120.000đ</span>
                                                <sub class="pricing-duration text-body font-medium-1 fw-bold">/năm</sub>
                                            </div>
                                            <small class="annual-pricing d-none text-muted"></small>
                                        </div>
                                        <ul class="list-group list-group-circle text-start">
                                            <li class="list-group-item">PayPal payments</li>
                                            <li class="list-group-item">Logic Jumps</li>
                                            <li class="list-group-item">File upload with 5GB storage</li>
                                            <li class="list-group-item">Custom domain support</li>
                                            <li class="list-group-item">Stripe integration</li>
                                        </ul>
                                        <button class="btn w-100 btn-outline-primary mt-2">Upgrade</button>
                                    </div>
                                </div>
                            </div>
                            <!--/ đồng plan -->

                            <!-- bạc plan -->
                            <div class="col-12 col-md-3">
                              <div class="card enterprise-pricing non-popular text-center">
                                  <div class="card-body">
                                      <img src="{{ asset('images/illustration/Pot3.svg') }}" class="mb-2"
                                          alt="svg img" />
                                      <h3>Bạc</h3>
                                      <p class="card-text">Solution for big organizations</p>
                                      <div class="annual-plan">
                                          <div class="plan-price mt-2">
                                              <span class="pricing-enterprise-value fw-bolder text-primary">300.000đ</span>
                                              <sub class="pricing-duration text-body font-medium-1 fw-bold">/năm</sub>
                                          </div>
                                          <small class="annual-pricing d-none text-muted"></small>
                                      </div>
                                      <ul class="list-group list-group-circle text-start">
                                          <li class="list-group-item">PayPal payments</li>
                                          <li class="list-group-item">Logic Jumps</li>
                                          <li class="list-group-item">File upload with 5GB storage</li>
                                          <li class="list-group-item">Custom domain support</li>
                                          <li class="list-group-item">Stripe integration</li>
                                      </ul>
                                      <button class="btn w-100 btn-outline-primary mt-2">Upgrade</button>
                                  </div>
                              </div>
                          </div>
                          <!--/ bạc plan -->
                        </div>
                    </div>
                </div>
                <!--/ pricing plan cards -->
            </div>
        </div>
    </section>
@endsection

@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset('js/scripts/pages/page-pricing.js') }}"></script>
@endsection

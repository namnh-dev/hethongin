@extends('layouts/contentLayoutMaster')

@section('title', 'Cấu hình đơn giá')

@section('content')
    <section class="form-control-repeater">
        <div class="row">
            <!-- Invoice repeater -->
            <div class="col-12">
                <ul class="nav nav-pills mb-2">
                    <!-- Account -->
                    <li class="nav-item">
                        <a class="nav-link" href="{{ asset('cau-hinh/nha-xuong') }}">
                            <i data-feather="user" class="font-medium-3 me-50"></i>
                            <span class="fw-bold">Thông tin xưởng</span>
                        </a>
                    </li>
                    <!-- security -->
                    <li class="nav-item">
                        <a class="nav-link active" href="{{ asset('cau-hinh/don-gia') }}">
                            <i data-feather="lock" class="font-medium-3 me-50"></i>
                            <span class="fw-bold">Cấu hình đơn giá</span>
                        </a>
                    </li>
                    <!-- billing and plans -->
                    <li class="nav-item d-none d-sm-block">
                        <a class="nav-link" href="{{ asset('page/account-settings-billing') }}">
                            <i data-feather="file-text" class="font-medium-3 me-50"></i>
                            <span class="fw-bold">Billings &amp; Plans</span>
                        </a>
                    </li>
                    <!-- notification -->
                    <li class="nav-item d-none d-sm-block">
                        <a class="nav-link" href="#">
                            <i data-feather="bell" class="font-medium-3 me-50"></i>
                            <span class="fw-bold">Notifications</span>
                        </a>
                    </li>
                    <!-- connection -->
                    <li class="nav-item d-none d-sm-block">
                        <a class="nav-link" href="#">
                            <i data-feather="link" class="font-medium-3 me-50"></i>
                            <span class="fw-bold">Connections</span>
                        </a>
                    </li>
                </ul>
                <div class="card">
                    <div class="card-body">
                        <form action="#" class="invoice-repeater">
                            <div class="row">
                                <div class="col-12">
                                    <button id="create-repeater" class="btn btn-icon btn-primary" type="button"
                                        data-repeater-create>
                                        <i data-feather="plus" class="me-25"></i>
                                    </button>
                                </div>
                            </div>
                            <div data-repeater-list="invoice" id="repeater-form" style="display: none">
                                <div data-repeater-item>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="fw-bold text-nowrap">
                                            <h4 class="justify-content-start">Cấu hình đơn giá</h4>
                                        </div>
                                        <div class="mb-1">
                                            <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete
                                                type="button">
                                                <i data-feather="minus" class="me-25"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row d-flex align-items-end">
                                        <div class="col-md-2 col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="kho-may-x">Khổ máy X</label>
                                                <input type="text" class="form-control" id="kho-may-x"
                                                    aria-describedby="kho-may-x" />
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="kho-may-y">Khổ máy Y</label>
                                                <input type="number" class="form-control" id="kho-may-y"
                                                    aria-describedby="kho-may-y" />
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="gia-may">Giá máy</label>
                                                <input type="number" class="form-control" id="gia-may"
                                                    aria-describedby="gia-may" />
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="gia-phu-troi-theo-mau">Giá phụ trội theo
                                                    màu</label>
                                                <input type="text" class="form-control" id="gia-phu-troi-theo-mau" />
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="color">Màu</label>
                                                <select class="form-select" id="color">
                                                    <option disabled selected>Chọn loại màu</option>
                                                    <option>Red</option>
                                                    <option>Blue</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                </div>
                            </div>
                            <div class="row d-flex align-items-end my-2 flex-wrap">
                                <div class="col-6 col-md-2">
                                    <button type="submit" class="btn btn-primary btn-block me-1 flex-fill text-nowrap w-100 ">Lưu thay
                                        đổi</button>
                                </div>
                                <div class="col-6 col-md-1">
                                    <button type="reset" class="btn btn-outline-secondary btn-block flex-fill w-100 ">Cài
                                        lại</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /Invoice repeater -->
        </div>
    </section>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
@endsection

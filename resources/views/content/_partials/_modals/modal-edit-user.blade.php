<!-- Edit User Modal -->
<div class="modal fade" id="editUser" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
        <div class="modal-content">
            <div class="modal-header bg-transparent">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body pb-5 px-sm-5 pt-50">
                <div class="text-center mb-2">
                    <h1 class="mb-1">Chỉnh sửa thông tin người dùng</h1>
                    <p>Cập nhật chi tiết người dùng sẽ giúp bảo mật quyền riêng tư.</p>
                </div>
                <form id="editUserForm" class="row gy-1 pt-75" action="{{ URL::to('/edit-profile') }}" method="POST">
                    @csrf
                    <div class="col-12 col-md-6">
                        <label class="form-label" for="modalEditUserUsername">Username</label>
                        <input type="text" id="modalEditUserUsername" name="modalEditUserUsername"
                            class="form-control" value="{{ Auth::user()->username }}" disabled />
                    </div>
                    <div class="col-12 col-md-6">
                        <label class="form-label" for="modalEditUserFullname">Full Name</label>
                        <input type="text" id="modalEditUserFullname" name="modalEditUserFullname"
                            class="form-control" placeholder="Enter your fullname" value="{{ Auth::user()->fullname }}"
                            data-msg="Please enter your full name" />
                        <span id="error-modalEditUserFullname" style="color: red; display: none"></span>
                    </div>
                    <div class="col-12 col-md-6">
                        <label class="form-label" for="modalEditPhone">Phone</label>
                        <input type="text" id="modalEditPhone" name="modalEditPhone" class="form-control"
                            value="{{ Auth::user()->phone }}" placeholder="Enter your phone number" disabled />
                    </div>
                    <div class="col-12 col-md-6">
                        <label class="form-label" for="modalEditEmail">Email</label>
                        <input type="text" id="modalEditEmail" name="modalEditEmail" class="form-control"
                            value="{{ Auth::user()->email }}" placeholder="Enter your email" />
                        <span id="error-modalEditEmail" style="color: red; display: none"></span>
                    </div>
                    <div class="col-12 col-md-6">
                        <label class="form-label" for="modalEditUserGender">Gender</label>
                        <select id="modalEditUserGender" name="modalEditUserGender" class="form-select"
                            aria-label="Default select example">
                            <option selected disabled>Select your gender</option>
                            <option value="1" {{ Auth::user()->gender == 1 ? 'selected' : '' }}>Male</option>
                            <option value="0" {{ Auth::user()->gender == 0 ? 'selected' : '' }}>Female</option>
                        </select>
                        <span id="error-modalEditUserGender" style="color: red; display: none"></span>
                    </div>
                    <div class="col-12 col-md-6">
                        <label class="form-label" for="pd-default">Dob</label>
                        <input type="text" id="pd-default" name="modalEditUserDob" class="form-control pickadate"
                            value="{{ \Carbon\Carbon::parse(Auth::user()->dob)->format('Y-m-d') }}"
                            placeholder="Select your dob" />
                        <span id="error-modalEditUserDob" style="color: red; display: none"></span>
                    </div>
                    <div class="col-12">
                        <label class="form-label" for="modalEditUserAddress">Address</label>
                        <input type="text" id="modalEditUserAddress" name="modalEditUserAddress" class="form-control"
                            placeholder="Enter your address" value="{{ Auth::user()->address }}" />
                        <span id="error-modalEditUserAddress" style="color: red; display: none"></span>
                    </div>
                    <div class="col-12 col-md-4">
                        <label class="form-label" for="modalEditUserCity">City</label>
                        <select id="modalEditUserCity" name="modalEditUserCity" class="select2 form-select">
                        </select>
                        <span id="error-modalEditUserCity" style="color: red; display: none"></span>
                    </div>
                    <div hidden id="city_id">{{ Auth::user()->city_id }}</div>
                    <div class="col-12 col-md-4">
                        <label class="form-label" for="modalEditUserProvince">Province</label>
                        <select id="modalEditUserProvince" name="modalEditUserProvince" class="select2 form-select">
                        </select>
                        <span id="error-modalEditUserProvince" style="color: red; display: none"></span>
                    </div>
                    <div hidden id="province_id">{{ Auth::user()->province_id }}</div>
                    <div class="col-12 col-md-4">
                        <label class="form-label" for="modalEditUserWard">Ward</label>
                        <select id="modalEditUserWard" name="modalEditUserWard" class="select2 form-select">
                        </select>
                        <span id="error-modalEditUserWard" style="color: red; display: none"></span>
                    </div>
                    <div hidden id="ward_id">{{ Auth::user()->ward_id }}</div>
                    <div class="col-12 text-center mt-2 pt-50">
                        <button type="submit" class="btn btn-primary me-1">Submit</button>
                        <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                            aria-label="Close">
                            Discard
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Alert-->
@if (Session::has('success'))
    <div id="success-alert" hidden>
        {{ Session::get('success') }}
    </div>
@endif

@if (Session::has('error'))
    <div id="error-alert" hidden>
        {{ Session::get('error') }}
    </div>
@endif
<!-- Alert-->

<!--/ Edit User Modal -->

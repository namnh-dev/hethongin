/******/
// (() => { // webpackBootstrap
//     var __webpack_exports__ = {};
//     /*!****************************************!*\
//       !*** ./resources/assets/js/scripts.js ***!
//       \****************************************/
//     (function(window, undefined) {
//         'use strict';

//         /*
//         NOTE:
//         ------
//         PLACE HERE YOUR OWN JAVASCRIPT CODE IF NEEDED
//         WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR JAVASCRIPT CODE PLEASE CONSIDER WRITING YOUR SCRIPT HERE.  */
//         $.getJSON('/data/bang-gia-giay.json', function(data) {
//             const $loai_giay_option = $("#loai-giay-option");
//             const $dinh_luong_giay_option = $("#dinh-luong-giay-option");
//             $loai_giay_option.change(function() {
//                 const selectedOption = $(this).val();
//                 var uniqueValues = [];
//                 $dinh_luong_giay_option.empty();
//                 data.forEach((option) => {
//                     if ($(this).val() == option.loai_giay) {
//                         if (uniqueValues.indexOf(option.dinh_luong) === -1) {
//                             uniqueValues.push(option.dinh_luong);
//                             $dinh_luong_giay_option.append($("<option>", {
//                                 value: option.dinh_luong,
//                                 text: option.dinh_luong
//                             }));
//                         }
//                     }
//                 });
//             });

//         });

//         $(document).ready(function() {
//             if ($('#newY').length) {
//                 $('#printerY').val($('#newY').text());
//             }
//             if ($('#newX').length) {
//                 $('#printerX').val($('#newX').text());
//             }
//             var kho_may_option = $('#kho-may-option').find(":selected").val();
//             var printerX;
//             var printerY;
//             switch (kho_may_option) {
//                 case '360x520':
//                     printerX = 360;
//                     printerY = 520;
//                     break;
//                 case '520x720':
//                     printerX = 520;
//                     printerY = 720;
//                     break;
//                 case '790x1090':
//                     printerX = 790;
//                     printerY = 1090;
//                     break;
//                 case 'optionXY':
//                     printerX = $('#X-input').val();
//                     printerY = $('#Y-input').val();
//                     break;
//             }
//             var printA = $('#chieu-ngang').val();
//             var printB = $('#chieu-doc').val();

//             // Khi chưa có giá trị thì k làm gì cả
//             if (!printerX || !printerY || !printA || !printB) {
//                 return;
//             }

//             $('#widthRect').html(`${printerX}mm`);
//             $('#heightRect').html(`${printerY}mm`);
//             var a = 0;
//             var b = 0;
//             var c = document.getElementById("myCanvas");
//             // lấy ra cạnh dài hơn
//             var max = Math.max(printerX, printerY);
//             // hiện tại em đang lấy ra cạnh Y
//             max = printerY;
//             console.log("max", max);
//             var denominator = 0;
//             let count = 1;
//             // trong khi chưa lớn hơn 500 thì count tăng lên 1 đơn vị, đến khi lớn hơn 500 thì count sẽ là mẫu số chung
//             while (denominator < 300) {
//                 denominator = max * count;
//                 count++;
//             }

//             console.log(denominator);
//             var miss = denominator / printerY;
//             console.log(miss);

//             if (miss !== 0) {
//                 printA = printA * miss;
//                 printB = printB * miss;
//                 printerX = printerX * miss;
//                 printerY = printerY * miss;
//             }
//             c.width = printerX;
//             c.height = printerY;
//             var ctx = c.getContext("2d");
//             var rev = true;
//             if ($('.checkEle').length) {
//                 $('#element').show();
//             }
//             if ($('#caseCoor').length) {
//                 if ($('.resultVerRev').length && $('.resultVer').length) {
//                     if (countAxBVerticalKeys() >= countAxBVerticalKeys(rev)) {
//                         printAxBVertical(rev);
//                         $(".resultVer").remove();
//                         $("#coordinate").html(`and get ${countAxBVerticalKeys(rev)} coordinate`);
//                     } else {
//                         printAxBVertical();
//                         $(".resultVerRev").remove();
//                         $("#coordinate").html(`and get ${countAxBVerticalKeys()} coordinate`);
//                     }
//                 } else if ($('.resultHorRev').length && $('.resultHor').length) {
//                     if (countAxBHorKeys() >= countAxBHorKeys(rev)) {
//                         printAxBHor(rev);
//                         console.log("horRev");
//                         $(".resultHor").remove();
//                         $("#coordinate").html(`and get ${countAxBHorKeys(rev)} coordinate`);
//                     } else {
//                         printAxBHor();
//                         console.log("hor");
//                         $(".resultHorRev").remove();
//                         $("#coordinate").html(`and get ${countAxBHorKeys()} coordinate`);
//                     }
//                 } else if ($('.resultHorRev').length && $('.resultVer').length) {
//                     if (countAxBHorKeys(rev) >= countAxBVerticalKeys()) {
//                         printAxBVertical();
//                         console.log("ver");
//                         $(".resultHorRev").remove();
//                         $("#coordinate").html(`and get ${countAxBVerticalKeys()} coordinate`);
//                     } else {
//                         printAxBHor(rev);
//                         console.log("horRev");
//                         $(".resultVer").remove();
//                         $("#coordinate").html(`and get ${countAxBHorKeys(rev)} coordinate`);
//                     }
//                 } else if ($('.resultVerRev').length && $('.resultHor').length) {
//                     if (countAxBHorKeys() >= countAxBVerticalKeys(rev)) {
//                         printAxBVertical(rev);
//                         console.log("verRev");
//                         $(".resultHor").remove();
//                         $("#coordinate").html(`and get ${countAxBVerticalKeys(rev)} coordinate`);
//                     } else {
//                         printAxBHor();
//                         console.log("hor");
//                         $(".resultVerRev").remove();
//                         $("#coordinate").html(`and get ${countAxBHorKeys()} coordinate`);
//                     }
//                 }
//                 $("#caseCoor").remove();
//             } else {
//                 if ($('.resultVer').length) {
//                     if ($('#reverse').length) {
//                         printAxBVertical(rev);
//                     } else {
//                         printAxBVertical();
//                     }
//                     countAxBVerticalKeys();
//                 } else if ($('.resultHor').length) {
//                     if ($('#reverse').length) {
//                         printAxBHor(rev);
//                     } else {
//                         printAxBHor();
//                     }
//                     countAxBHorKeys();
//                 } else if ($('.resultHor2').length) {
//                     if ($('#reverse').length) {
//                         printAxBHor(rev);
//                     } else {
//                         printAxBHor();
//                     }

//                 } else if ($('.resultVer2').length) {
//                     if ($('#reverse').length) {
//                         printAxBVertical(rev);
//                     } else {
//                         printAxBVertical();
//                     }
//                 }
//             }
//             // }

//             function printAxBVertical(rev) {
//                 a = 0;
//                 b = 0;
//                 if (rev) {
//                     printA = $('#chieu-doc').val() * miss;
//                     printB = $('#chieu-ngang').val() * miss;
//                     $('#chieu-ngang').val(printA / miss);
//                     $('#chieu-doc').val(printB / miss);
//                 } else {
//                     printA = $('#chieu-ngang').val() * miss;
//                     printB = $('#chieu-doc').val() * miss;
//                 }
//                 /* -------------In AxB dọc------------- */
//                 var toXpColumn = Math.floor(printerX / printA); //số cột vẽ được
//                 var toXpRow = Math.floor(printerY / printB); //số hàng vẽ được
//                 for (let i = 0; i < toXpRow; i++) {
//                     ctx.strokeRect(a, b, printA, printB);
//                     for (let j = 0; j < toXpColumn; j++) {
//                         ctx.strokeRect(a, b, printA, printB);
//                         a += Number(printA);
//                     }
//                     a = 0;
//                     b += Number(printB);
//                 }
//                 var toXpColumnRemainder = Number(printerX); //cột còn thừa
//                 var toXpRowRemainder = printerY % printB; //hàng còn thừa
//                 if (!$('.resultVer2').length && !$('.resultHor2').length) {
//                     fillRectHorizontal(toXpColumnRemainder, toXpRowRemainder, printA, printB, toXpRow);
//                 }
//                 /* -------------In AxB dọc------------- */

//                 /* -------------Đánh số a to x------------- */
//                 var halfA = printA / 2;
//                 var halfB = printB / 2;
//                 var jump = halfA * 2;
//                 var coordinateA = halfA;
//                 var index = 1;
//                 var textWidth = ctx.measureText(index).width;
//                 for (let k = 0; k < toXpColumn; k++) {
//                     for (let t = 0; t < toXpRow; t++) {
//                         ctx.fillStyle = "#003300";
//                         ctx.font = '15px san-serif';
//                         ctx.fillText(index, coordinateA - (textWidth / 2), halfB);
//                         // console.log(index, coordinateA, halfB)
//                         index++;
//                         halfB += printB;
//                     }
//                     halfB = printB / 2;
//                     coordinateA += jump;
//                     halfA = coordinateA;
//                 }
//                 var so_khoi_doc = index - 1;
//                 /* -------------Đánh số a to x------------- */
//                 if (!$('.resultVer2').length && !$('.resultHor2').length) {
//                     var tong_so_khoi = numberedRectHorizontal(toXpColumnRemainder, toXpRowRemainder, printA, printB, index, toXpRow) - 1;
//                 }
//                 if (tong_so_khoi != undefined) {
//                     $('#so-khoi-in-span').text(tong_so_khoi);
//                     $('#so-khoi-ngang-span').text(tong_so_khoi - so_khoi_doc);
//                     $('#so-khoi-doc-span').text(so_khoi_doc);
//                 } else {
//                     $('#so-khoi-in-span').text(so_khoi_doc);
//                     $('#so-khoi-ngang-span').text(0);
//                     $('#so-khoi-doc-span').text(so_khoi_doc);
//                 }
//             }

//             function countAxBVerticalKeys(rev) {
//                 a = 0;
//                 b = 0;
//                 if (rev === true) {
//                     printA = $('#chieu-doc').val() * miss;
//                     printB = $('#chieu-ngang').val() * miss;
//                 } else {
//                     printA = $('#chieu-ngang').val() * miss;
//                     printB = $('#chieu-doc').val() * miss;
//                 }
//                 console.log("hehe" + printA);
//                 /* -------------In AxB dọc------------- */
//                 var toXpColumn = Math.floor(printerX / printA); //số cột vẽ được
//                 var toXpRow = Math.floor(printerY / printB); //số hàng vẽ được
//                 var keys = [];
//                 for (let i = 0; i < toXpRow; i++) {
//                     // ctx.strokeRect(a, b, printA, printB);
//                     for (let j = 0; j < toXpColumn; j++) {
//                         // ctx.strokeRect(a, b, printA, printB);
//                         /*--------------Get coordinate vertical--------------*/
//                         var coord1 = `${a}-${b}`;
//                         var coord2 = `${Number(a + printA)}-${b}`;
//                         var coord3 = `${a}-${Number(printB + b)}`;
//                         var coord4 = `${Number(a + printA)}-${Number(printB + b)}`;
//                         const key1 = keys.indexOf(coord1);
//                         const key2 = keys.indexOf(coord2);
//                         const key3 = keys.indexOf(coord3);
//                         const key4 = keys.indexOf(coord4);
//                         keys.push(coord1);
//                         if (key1 > -1) {
//                             keys.splice(key1, 1);
//                         }
//                         keys.push(coord2);
//                         if (key2 > -1) {
//                             keys.splice(key2, 1);
//                         }
//                         keys.push(coord3);
//                         if (key3 > -1) {
//                             keys.splice(key3, 1);
//                         }
//                         keys.push(coord4);
//                         if (key4 > -1) {
//                             keys.splice(key4, 1);
//                         }
//                         /*--------------Get coordinate vertical--------------*/
//                         a += Number(printA);
//                     }
//                     a = 0;
//                     b += Number(printB);
//                 }
//                 var toXpColumnRemainder = Number(printerX); //cột còn thừa
//                 var toXpRowRemainder = printerY % printB; //hàng còn thừa
//                 countFillRectHorizontal(toXpColumnRemainder, toXpRowRemainder, printA, printB, toXpRow, ...keys)
//                     /* -------------In AxB dọc------------- */
//                 console.log(`${keys.length}keyver`)
//                 return keys.length;
//             }

//             function countAxBHorKeys(rev) {
//                 a = 0;
//                 b = 0;
//                 if (rev === true) {
//                     printA = $('#chieu-doc').val() * miss;
//                     printB = $('#chieu-ngang').val() * miss;
//                 } else {
//                     printA = $('#chieu-ngang').val() * miss;
//                     printB = $('#chieu-doc').val() * miss;
//                 }
//                 /* -------------In AxB ngang------------- */
//                 var toYpRow = Math.floor(printerY / printA);
//                 var toYpColumn = Math.floor(printerX / printB);
//                 var keys = [];
//                 for (let i = 0; i < toYpRow; i++) {
//                     // ctx.strokeRect(a, b, printB, printA);
//                     for (let j = 0; j < toYpColumn; j++) {
//                         // ctx.strokeRect(a, b, printB, printA);
//                         /*--------------Get coordinate horizontal--------------*/
//                         var coord1 = `${a}-${b}`;
//                         var coord2 = `${Number(a + printB)}-${b}`;
//                         var coord3 = `${a}-${Number(printA + b)}`;
//                         var coord4 = `${Number(a + printB)}-${Number(printA + b)}`;
//                         const key1 = keys.indexOf(coord1);
//                         const key2 = keys.indexOf(coord2);
//                         const key3 = keys.indexOf(coord3);
//                         const key4 = keys.indexOf(coord4);
//                         keys.push(coord1);
//                         if (key1 > -1) {
//                             keys.splice(key1, 1);
//                         }
//                         keys.push(coord2);
//                         if (key2 > -1) {
//                             keys.splice(key2, 1);
//                         }
//                         keys.push(coord3);
//                         if (key3 > -1) {
//                             keys.splice(key3, 1);
//                         }
//                         keys.push(coord4);
//                         if (key4 > -1) {
//                             keys.splice(key4, 1);
//                         }
//                         /*--------------Get coordinate horizontal--------------*/
//                         a += Number(printB);
//                     }
//                     a = 0;
//                     b += Number(printA);
//                 }
//                 var toYpColumnRemainder = printerX % printB; //cột còn thừa
//                 var toYpRowRemainder = Number(printerY); //hàng còn thừa
//                 countFillRectVertical(toYpColumnRemainder, toYpRowRemainder, printA, printB, ...keys);
//                 console.log(`${keys.length}keyhor`)
//                 return keys.length;
//                 /* -------------In AxB ngang------------- */
//             }

//             function printAxBHor(rev) {
//                 a = 0;
//                 b = 0;
//                 if (rev == true) {
//                     printA = $('#chieu-doc').val() * miss;
//                     printB = $('#chieu-ngang').val() * miss;
//                     $('#chieu-ngang').val(printA / miss);
//                     $('#chieu-doc').val(printB / miss);
//                 } else {
//                     printA = $('#chieu-ngang').val() * miss;
//                     printB = $('#chieu-doc').val() * miss;
//                 }
//                 /* -------------In AxB ngang------------- */
//                 var toYpRow = Math.floor(printerY / printA);
//                 var toYpColumn = Math.floor(printerX / printB);
//                 for (let i = 0; i < toYpRow; i++) {
//                     ctx.strokeRect(a, b, printB, printA);
//                     for (let j = 0; j < toYpColumn; j++) {
//                         ctx.strokeRect(a, b, printB, printA);
//                         a += Number(printB);
//                     }
//                     a = 0;
//                     b += Number(printA);
//                 }
//                 var toYpColumnRemainder = printerX % Math.floor(printB); //cột còn thừa
//                 var toYpRowRemainder = Number(printerY); //hàng còn thừa
//                 if (!$('.resultVer2').length && !$('.resultHor2').length) {
//                     fillRectVertical(toYpColumnRemainder, toYpRowRemainder, printA, printB, toYpColumn);
//                 }
//                 /* -------------In AxB ngang------------- */

//                 /* -------------Đánh số b to x------------- */
//                 var halfA = printA / 2;
//                 var halfB = printB / 2;
//                 var jump = halfB * 2;
//                 var coordinateB = halfB;
//                 var index = 1;
//                 var textWidth = ctx.measureText(index).width;
//                 for (let k = 0; k < toYpColumn; k++) {
//                     for (let t = 0; t < toYpRow; t++) {
//                         ctx.fillStyle = "#003300";
//                         ctx.font = '15px san-serif';
//                         ctx.fillText(index, coordinateB - (textWidth / 2), halfA);
//                         index++;
//                         halfA += printA;
//                     }
//                     halfA = printA / 2;
//                     coordinateB += jump;
//                     halfB = coordinateB;
//                 }
//                 var so_khoi_ngang = index - 1;
//                 /* -------------Đánh số b to x------------- */

//                 if (!$('.resultVer2').length && !$('.resultHor2').length) {
//                     var tong_so_khoi = numberedRectVertical(toYpColumnRemainder, toYpRowRemainder, printA, printB, index, toYpColumn) - 1;
//                 }
//                 if (tong_so_khoi != undefined) {
//                     $('#so-khoi-in-span').text(tong_so_khoi);
//                     $('#so-khoi-ngang-span').text(so_khoi_ngang);
//                     $('#so-khoi-doc-span').text(tong_so_khoi - so_khoi_ngang);
//                 } else {
//                     $('#so-khoi-in-span').text(so_khoi_ngang);
//                     $('#so-khoi-ngang-span').text(so_khoi_ngang);
//                     $('#so-khoi-doc-span').text(0);
//                 }
//             }

//             function fillRectHorizontal(newPrinterX, newPrinterY, printA, printB, toXpRow) {
//                 var a = 0;
//                 var b = Number(printB) * toXpRow;
//                 /* -------------In AxB ngang------------- */
//                 if (newPrinterY >= printA) {
//                     var newToXpColumn = Math.floor(newPrinterX / printB); //số cột mới vẽ được
//                     var newToXpRow = Math.floor(newPrinterY / printA); //số hàng mới vẽ được
//                     console.log(newToXpColumn, newToXpRow);
//                     for (let i = 0; i < newToXpRow; i++) {
//                         ctx.strokeRect(a, b, printB, printA);
//                         for (let j = 0; j < newToXpColumn; j++) {
//                             ctx.strokeRect(a, b, printB, printA);
//                             a += Number(printB);
//                         }
//                         a = 0;
//                         b += Number(printA);
//                     }
//                 }
//                 return newToXpRow;
//                 /* -------------In AxB ngang------------- */
//             }

//             function fillRectVertical(newPrinterX, newPrinterY, printA, printB, toYpColumn) {
//                 var a = Number(printB) * toYpColumn;
//                 var b = 0;
//                 /* -------------In AxB dọc------------- */
//                 if (newPrinterX >= printA) {
//                     var newToXpColumn = Math.floor(newPrinterX / printA); //số cột mới vẽ được
//                     var newToXpRow = Math.floor(newPrinterY / printB); //số hàng mới vẽ được
//                     console.log(newToXpColumn, newToXpRow);
//                     for (let i = 0; i < newToXpRow; i++) {
//                         for (let j = 0; j < newToXpColumn; j++) {
//                             ctx.strokeRect(a, b, printA, printB);
//                             a += Number(printA);
//                         }
//                         a = Number(printB) * toYpColumn;
//                         ctx.strokeRect(a, b, printA, printB);
//                         b += Number(printB);
//                     }
//                 }
//                 return newToXpColumn;
//                 /* -------------In AxB dọc------------- */
//             }

//             function numberedRectVertical(newPrinterX, newPrinterY, printA, printB, index, toYpColumn) {
//                 var newToXpColumn = Math.floor(newPrinterX / printA); //số cột mới vẽ được
//                 var newToXpRow = Math.floor(newPrinterY / printB); //số hàng mới vẽ được
//                 var halfA = printA / 2;
//                 var halfB = printB / 2;
//                 var jump = halfA * 2;
//                 var coordinateA = halfA;
//                 var textWidth = ctx.measureText(index).width;
//                 if (newPrinterX >= printA) {
//                     for (let k = 0; k < newToXpColumn; k++) {
//                         for (let t = 0; t < newToXpRow; t++) {
//                             ctx.fillStyle = "#003300";
//                             ctx.font = '15px san-serif';
//                             ctx.fillText(index, coordinateA + printB * toYpColumn - (textWidth / 2), halfB);
//                             console.log(index, coordinateA, halfB)
//                             index++;
//                             halfB += printB;
//                         }
//                         halfB = printB / 2;
//                         coordinateA += jump;
//                         halfA = coordinateA;
//                     }
//                 }
//                 return index;
//             }

//             function numberedRectHorizontal(newPrinterX, newPrinterY, printA, printB, index, toXpRow) {
//                 var newToXpColumn = Math.floor(newPrinterX / printB); //số cột mới vẽ được
//                 var newToXpRow = Math.floor(newPrinterY / printA); //số hàng mới vẽ được
//                 var halfA = printA / 2;
//                 var halfB = printB / 2;
//                 var jump = halfB * 2;
//                 var coordinateB = halfB;
//                 var textWidth = ctx.measureText(index).width;
//                 if (newPrinterY >= printA) {
//                     for (let k = 0; k < newToXpColumn; k++) {
//                         for (let t = 0; t < newToXpRow; t++) {
//                             ctx.fillStyle = "#003300";
//                             ctx.font = '15px san-serif';
//                             ctx.fillText(index, coordinateB - (textWidth / 2), halfA + printB * toXpRow);
//                             console.log(index, coordinateB, halfA)
//                             index++;
//                             halfA += printA;
//                         }
//                         halfA = printA / 2;
//                         coordinateB += jump;
//                         halfB = coordinateB;
//                     }
//                 }
//                 return index;
//             }

//             function countFillRectVertical(newPrinterX, newPrinterY, printA, printB, toYpColumn, ...keys) {
//                 var a = Number(printB) * toYpColumn;
//                 var b = 0;
//                 /* -------------In AxB dọc------------- */
//                 if (newPrinterX >= printA) {
//                     var newToXpColumn = Math.floor(newPrinterX / printA); //số cột mới vẽ được
//                     var newToXpRow = Math.floor(newPrinterY / printB); //số hàng mới vẽ được
//                     for (let i = 0; i < newToXpRow; i++) {
//                         for (let j = 0; j < newToXpColumn; j++) {
//                             // ctx.strokeRect(a, b, printA, printB);
//                             /*--------------Get coordinate vertical--------------*/
//                             var coord1 = `${a}-${b}`;
//                             var coord2 = `${Number(a + printA)}-${b}`;
//                             var coord3 = `${a}-${Number(printB + b)}`;
//                             var coord4 = `${Number(a + printA)}-${Number(printB + b)}`;
//                             const key1 = keys.indexOf(coord1);
//                             const key2 = keys.indexOf(coord2);
//                             const key3 = keys.indexOf(coord3);
//                             const key4 = keys.indexOf(coord4);
//                             keys.push(coord1);
//                             if (key1 > -1) {
//                                 keys.splice(key1, 1);
//                             }
//                             keys.push(coord2);
//                             if (key2 > -1) {
//                                 keys.splice(key2, 1);
//                             }
//                             keys.push(coord3);
//                             if (key3 > -1) {
//                                 keys.splice(key3, 1);
//                             }
//                             keys.push(coord4);
//                             if (key4 > -1) {
//                                 keys.splice(key4, 1);
//                             }
//                             /*--------------Get coordinate vertical--------------*/
//                             a += Number(printA);
//                         }
//                         a = Number(printB) * toYpColumn;
//                         ctx.strokeRect(a, b, printA, printB);
//                         b += Number(printB);
//                     }
//                 }
//                 /* -------------In AxB dọc------------- */
//             }

//             function countFillRectHorizontal(newPrinterX, newPrinterY, printA, printB, toXpRow, ...keys) {
//                 var a = 0;
//                 var b = Number(printB) * toXpRow;
//                 /* -------------In AxB ngang------------- */
//                 if (newPrinterY >= printA) {
//                     var newToXpColumn = Math.floor(newPrinterX / printB); //số cột mới vẽ được
//                     var newToXpRow = Math.floor(newPrinterY / printA); //số hàng mới vẽ được
//                     // console.log(newToXpColumn, newToXpRow);
//                     for (let i = 0; i < newToXpRow; i++) {
//                         // ctx.strokeRect(a, b, printB, printA);
//                         for (let j = 0; j < newToXpColumn; j++) {
//                             /*--------------Get coordinate horizontal--------------*/
//                             var coord1 = `${a}-${b}`;
//                             var coord2 = `${Number(a + printB)}-${b}`;
//                             var coord3 = `${a}-${Number(printA + b)}`;
//                             var coord4 = `${Number(a + printB)}-${Number(printA + b)}`;
//                             const key1 = keys.indexOf(coord1);
//                             const key2 = keys.indexOf(coord2);
//                             const key3 = keys.indexOf(coord3);
//                             const key4 = keys.indexOf(coord4);
//                             keys.push(coord1);
//                             if (key1 > -1) {
//                                 keys.splice(key1, 1);
//                             }
//                             keys.push(coord2);
//                             if (key2 > -1) {
//                                 keys.splice(key2, 1);
//                             }
//                             keys.push(coord3);
//                             if (key3 > -1) {
//                                 keys.splice(key3, 1);
//                             }
//                             keys.push(coord4);
//                             if (key4 > -1) {
//                                 keys.splice(key4, 1);
//                             }
//                             /*--------------Get coordinate horizontal--------------*/
//                             // ctx.strokeRect(a, b, printB, printA);
//                             a += Number(printB);
//                         }
//                         a = 0;
//                         b += Number(printA);
//                     }
//                 }
//                 /* -------------In AxB ngang------------- */
//             }

//             if ($('#reverse').length) {
//                 var printAx = $('#chieu-ngang').val() * miss;
//                 var printBx = $('#chieu-doc').val() * miss;
//                 $('#chieu-ngang').val(printAx / miss);
//                 $('#chieu-doc').val(printBx / miss);
//             }

//             /* --------Show data--------- */
//             $('#kich-thuoc-span').text(printA + "x" + printB);
//             $('#so-luong-span').text($('#so-luong').val());
//             $('#mat-in-span').text($('#so-mat-in').find(":selected").val());
//             /* --------Show data--------- */
//         });
//     })(window);
//     /******/
// })();